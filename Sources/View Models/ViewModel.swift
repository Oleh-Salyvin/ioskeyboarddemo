//
//  ViewModel.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 11/26/18.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


final class ViewModel
{
    enum Name {
        case ScrollViewLight, ScrollViewLightMix, ScrollViewHardMix, ScrollViewLongContentSize, ScrollViewOther
        
        case TableViewLight
        
        case PostView, LabelView, TextFieldView
    }
    
    required init() {}
    
    static let sharedInstance = ViewModel()
    
    func context(name: Name) -> ViewContextProtocol
    {
        switch name {
        case .ScrollViewLight:
            return ScrollViewLightViewContext(viewModel: self)
        case .ScrollViewLightMix:
            return ScrollViewLightMixViewContext(viewModel: self)
        case .ScrollViewHardMix:
            return ScrollViewHardMixViewContext(viewModel: self)
        case .ScrollViewLongContentSize:
            return ScrollViewLongContentSizeViewContext(viewModel: self)
        case .ScrollViewOther:
            return OtherViewContext(viewModel: self)
        case .TableViewLight:
            return TableViewLightViewContext(viewModel: self)
        case .PostView:
            return PostViewContext(viewModel: self)
        case .LabelView:
            return LabelViewContext(viewModel: self)
        case .TextFieldView:
            return TextFieldViewContext(viewModel: self)
        }
    }
}
