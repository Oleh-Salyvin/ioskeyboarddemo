//
//  ViewContextProtocol.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 11/26/18.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation

protocol ViewContextProtocol
{
    init(viewModel: ViewModel)
}
