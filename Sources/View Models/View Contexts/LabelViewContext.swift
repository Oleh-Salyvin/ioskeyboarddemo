//
//  LabelViewContext.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 15.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


final class LabelViewContext: ViewContextProtocol
{
    required init(viewModel: ViewModel)
    {
        self.viewModel = viewModel
    }
    
    unowned let viewModel: ViewModel
    
    func text(index: Int) -> String
    {
        return "\(index): some\ntext"
    }
}
