//
//  ScrollViewLightMixViewContext.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 11.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


class ScrollViewLightMixViewContext: ViewContextProtocol
{
    required init(viewModel: ViewModel)
    {
        self.viewModel = viewModel
    }
    
    unowned let viewModel: ViewModel
    
    var labelViewContext: LabelViewContext = ViewModel.sharedInstance.context(name: .LabelView) as! LabelViewContext
    
    var textFieldViewContext: TextFieldViewContext = ViewModel.sharedInstance.context(name: .TextFieldView) as! TextFieldViewContext
    
    var personGroupContext: PersonGroupContext = PersonGroupContext()
    
    func navigationItemTitle() -> String
    {
        return "ScrollView light mix"
    }
}
