//
//  ScrollViewLongContentSizeViewContext.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 12.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


final class ScrollViewLongContentSizeViewContext: ViewContextProtocol
{
    required init(viewModel: ViewModel)
    {
        self.viewModel = viewModel
    }
    
    unowned let viewModel: ViewModel
    
    func navigationItemTitle() -> String
    {
        return "ScrollView LongContentSize"
    }
}
