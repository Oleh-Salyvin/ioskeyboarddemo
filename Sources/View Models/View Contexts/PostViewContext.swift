//
//  PostViewContext.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 14.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation
import CoreData


final class PostViewContext: ViewContextProtocol
{
    required init(viewModel: ViewModel)
    {
        self.viewModel = viewModel
        
        self.fetchedResultsController = CoreModel.sharedInstance.personFetchedResultsController()
    }
    
    unowned let viewModel: ViewModel
    
    func textPlaceholder(indexPath: IndexPath) -> String
    {
        return String(indexPath.row)
    }
    
    //MARK: - CoreData
    
    fileprivate(set) var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>? = nil
    
    func performFetch()
    {
        do {
            try self.fetchedResultsController?.performFetch()
        }
        catch let error {
            print("\(error)")
        }
    }
    
    var fetchedObjects: [Any]? {
        return self.fetchedResultsController?.fetchedObjects
    }
    
    func numberOfSections() -> Int
    {
        if self.fetchedResultsController?.sections != nil
        {
            return self.fetchedResultsController!.sections!.count
        }
        
        return 0
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int
    {
        if self.fetchedResultsController?.sections != nil
        {
            return self.fetchedResultsController!.sections![section].numberOfObjects
        }
        
        return 0
    }
    
    func insertPosts(number: Int)
    {
        for i in 0..<number
        {
            self.insertPost(date: NSDate(), title: String(i), text: "text")
        }
    }
    
    func insertPost(date: NSDate, title: String, text: String)
    {
        guard let post: PostEntity = CoreModel.sharedInstance.insertPost() else {
            return
        }
        
        post.date = date
        post.title = title
        post.text = text
    }
    
    func save(_ closure: @escaping doneWithErrorClosure)
    {
        CoreModel.sharedInstance.save(closure)
    }
}
