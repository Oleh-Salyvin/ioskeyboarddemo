//
//  TableViewLightViewContext.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 11.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


final class TableViewLightViewContext: ViewContextProtocol
{
    required init(viewModel: ViewModel)
    {
        self.viewModel = viewModel
    }
    
    unowned let viewModel: ViewModel
    
    var viewContext: PostViewContext = ViewModel.sharedInstance.context(name: .PostView) as! PostViewContext
    
    var navigationItemTitle = "TableView light"
    
    func textFieldPlaceholder(at indexPath: IndexPath) -> String
    {
        return self.viewContext.textPlaceholder(indexPath: indexPath)
    }
    
    func textViewText(at indexPath: IndexPath) -> String
    {
        return String(indexPath.row + 1)
    }
}
