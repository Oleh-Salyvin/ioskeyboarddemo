//
//  OtherViewContext.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 15.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


final class OtherViewContext: ViewContextProtocol
{
    required init(viewModel: ViewModel)
    {
        self.viewModel = viewModel
    }
    
    unowned let viewModel: ViewModel
    
    var textFieldLabel: String {
        return "some text"
    }
    
    var timeNow: String {
        return "Time: \(DispatchTime.now().uptimeNanoseconds)"
    }
    
    var wifiAddressLabel: String {
        return "Wi-Fi IP: " + self.getWIFIAddress()
    }
    
    private func getWIFIAddress() -> String
    {
        var address: String = "?"
        
        var ifaddr: UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return address }
        guard let firstAddr = ifaddr else { return address }
        
        for ifptr in sequence(first: firstAddr, next: { ($0).pointee.ifa_next })
        {
            let interface = ifptr.pointee
            
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6)
            {
                let name = String(cString: interface.ifa_name)
                if name == "en0"
                {
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                    
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
}
