//
//  UIConfiguration.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 28.03.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class UIConfiguration
{
    // UIViewController
    var automaticallyAdjustsScrollViewInsets: Bool = true
    
    /// UINavigationController
    var hidesBottomBarWhenPushed: Bool = false
    var hidesBarsOnSwipe: Bool = false
    var hidesBarsOnTap: Bool = false
    var hidesBarsWhenKeyboardAppears: Bool = false
    var hidesBarsWhenVerticallyCompact: Bool = false
    var setToolbarHidden: (hidden: Bool, animated: Bool) = (hidden: true, animated: false)
    
    // UIScrollView
    var bounceOnScroll: Bool = true
    var bounceOnZoom: Bool = true
    var bounceHorizontally: Bool = false
    var bounceVertically: Bool = false
    
    var contentInsetAdjustmentBehavior: ContentInsetAdjustmentBehavior = .automatic
    var keyboardDismissMode: UIScrollView.KeyboardDismissMode = .none
    
    var viewHighlightBorder: Bool = false
    var viewHighlightBorderLayer: View = View(borderWidth: 2, borderColor: UIColor.red)
    
    // KeyboardManagement
    var attributedAlignment: SVAttributedAlignment = .init()
    
    struct View
    {
        let borderWidth: CGFloat
        
        let borderColor: UIColor
    }
}


enum ContentInsetAdjustmentBehavior : Int {
    
    
    case automatic // Similar to .scrollableAxes, but for backward compatibility will also adjust the top & bottom contentInset when the scroll view is owned by a view controller with automaticallyAdjustsScrollViewInsets = YES inside a navigation controller, regardless of whether the scroll view is scrollable
    
    case scrollableAxes // Edges for scrollable axes are adjusted (i.e., contentSize.width/height > content.size.width/height or alwaysBounceHorizontal/Vertical = YES)
    
    case never // contentInset is not adjusted
    
    case always // contentInset is always adjusted by the scroll view's safeAreaInsets
}

