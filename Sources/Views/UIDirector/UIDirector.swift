//
//  UIDirector.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 01.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class UIDirector
{
    enum WorkType
    {
        case construct
        
        case configure
        
        case reset
    }
    
    // ScrollView
    
    func constructScrollViewMenuViewController(_ builder: ScrollViewMenuViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(navigationController: builder.viewController.navigationController!)
            
        case .reset:
            builder.reset()
        }
    }
    
    func constructScrollViewLightViewController(_ builder: ScrollViewLightViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
        
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(scrollView: builder.scrollView)
            builder.construct(view: builder.scrollView)
        
        case .reset:
            builder.reset()
        }
    }
    
    func constructScrollViewLightFullScreenViewController(_ builder: ScrollViewLightMixViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(scrollView: builder.scrollView)
            builder.construct(view: builder.scrollView)
            
        case .reset:
            builder.reset()
        }
    }
    
    func constructScrollViewLightMixViewController(_ builder: ScrollViewHardViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(scrollView: builder.scrollView)
            builder.construct(view: builder.scrollView)
            
        case .reset:
            builder.reset()
        }
    }
    
    func constructScrollViewHardMixViewController(_ builder: ScrollViewHardMixViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(scrollView: builder.scrollView)
            builder.construct(view: builder.scrollView)
            
        case .reset:
            builder.reset()
        }
    }
    
    func constructScrollViewLongContentSizeViewController(_ builder: ScrollViewLongContentSizeViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(scrollView: builder.scrollView)
            builder.construct(view: builder.scrollView)
            
        case .reset:
            builder.reset()
        }
    }
    
    // CollectionView
    
    func constructCollectionViewMenuViewController(_ builder: CollectionViewMenuViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct()
            
        case .reset:
            builder.reset()
        }
    }
    
    func constructCollectionViewLightViewController(_ builder: CollectionViewLightViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(collectionView: builder.collectionView)
            builder.construct(view: builder.collectionView)
            
        case .reset:
            builder.reset()
        }
    }
    
    func constructCollectionViewHardMixViewController(_ builder: CollectionViewHardMixViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(collectionView: builder.collectionView)
            builder.construct(view: builder.collectionView)
            
        case .reset:
            builder.reset()
        }
    }
    
    // TableView
    
    func constructTableViewMenuViewController(_ builder: TableViewMenuViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct()
            
        case .reset:
            builder.reset()
        }
    }
    
    func constructTableViewLightViewController(_ builder: TableViewLightViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(tableView: builder.tableView)
            builder.construct(view: builder.tableView)
            
        case .reset:
            builder.reset()
        }
    }
    
    @available(iOS, unavailable)
    func constructTableViewLightMixViewController(_ builder: TableViewLightMixViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(tableView: builder.tableView)
            builder.construct(view: builder.tableView)
            
        case .reset:
            builder.reset()
        }
    }
    
    func constructTableViewLightTableViewController(_ builder: TableViewLightTableViewControllerBuilder, workType: WorkType)
    {
        switch workType {
        case .construct:
            builder.construct()
            
        case .configure:
            builder.construct(viewController: builder.viewController)
            builder.construct(navigationController: builder.viewController.navigationController!)
            builder.construct(navigationItem: builder.viewController.navigationController!.navigationItem)
            builder.construct(tableView: builder.tableView)
            builder.construct(view: builder.tableView)
            
        case .reset:
            builder.reset()
        }
    }
}
