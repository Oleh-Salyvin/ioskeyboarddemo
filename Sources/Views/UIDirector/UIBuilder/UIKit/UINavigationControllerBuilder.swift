//
//  UINavigationControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 01.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class UINavigationControllerBuilder
{
    init(navigationController: UINavigationController, configuration: UIConfiguration)
    {
        self.navigationController = navigationController
        
        self.configuration = configuration
    }
    
    var navigationController: UINavigationController
    
    var configuration: UIConfiguration
    
    func construct()
    {
        self.navigationController.hidesBottomBarWhenPushed = self.configuration.hidesBottomBarWhenPushed
        self.navigationController.hidesBarsOnSwipe = self.configuration.hidesBarsOnSwipe
        self.navigationController.hidesBarsOnTap = self.configuration.hidesBarsOnTap
        self.navigationController.hidesBarsWhenKeyboardAppears = self.configuration.hidesBarsWhenKeyboardAppears
        self.navigationController.hidesBarsWhenVerticallyCompact = self.configuration.hidesBarsWhenVerticallyCompact
        
        self.navigationController.setToolbarHidden(self.configuration.setToolbarHidden.hidden, animated: self.configuration.setToolbarHidden.animated)
    }
    
    func reset()
    {
        self.navigationController.hidesBottomBarWhenPushed = false
        self.navigationController.hidesBarsOnSwipe = false
        self.navigationController.hidesBarsOnTap = false
        self.navigationController.hidesBarsWhenKeyboardAppears = false
        self.navigationController.hidesBarsWhenVerticallyCompact = false
        
        self.navigationController.navigationItem.leftBarButtonItems = nil
        self.navigationController.navigationItem.rightBarButtonItems = nil
        
        self.navigationController.navigationItem.titleView = nil
        self.navigationController.navigationItem.title = nil
        
        self.navigationController.setToolbarHidden(true, animated: false)
    }
}
