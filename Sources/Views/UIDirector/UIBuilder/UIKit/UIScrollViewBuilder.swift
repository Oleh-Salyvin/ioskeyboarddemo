//
//  UIScrollViewBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 01.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class UIScrollViewBuilder
{
    required init(scrollView: UIScrollView, configuration: UIConfiguration)
    {
        self.scrollView = scrollView
        
        self.configuration = configuration
    }
    
    var scrollView: UIScrollView
    
    var configuration: UIConfiguration
    
    func construct()
    {
        if #available(iOS 11.0, *)
        {
            self.scrollView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior(rawValue: self.configuration.contentInsetAdjustmentBehavior.rawValue)!
        }
    }
    
    func result() -> UIScrollView
    {
        return self.scrollView
    }
    
    func reset()
    {
        if #available(iOS 11.0, *)
        {
            self.scrollView.contentInsetAdjustmentBehavior = .automatic
        }
    }
}
