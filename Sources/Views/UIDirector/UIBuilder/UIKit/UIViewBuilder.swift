//
//  UIViewBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 01.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class UIViewBuilder
{
    required init(view: UIView, configuration: UIConfiguration)
    {
        self.view = view
        
        self.configuration = configuration
    }
    
    var view: UIView
    
    var configuration: UIConfiguration
    
    func construct()
    {
        if self.configuration.viewHighlightBorder
        {
            self.view.layer.borderWidth = self.configuration.viewHighlightBorderLayer.borderWidth
            self.view.layer.borderColor = self.configuration.viewHighlightBorderLayer.borderColor.cgColor
        }
    }
    
    func reset()
    {
        self.view.layer.borderWidth = 1
        self.view.layer.borderColor = UIColor.clear.cgColor
    }
    
    func result() -> UIView
    {
        return self.view
    }
}
