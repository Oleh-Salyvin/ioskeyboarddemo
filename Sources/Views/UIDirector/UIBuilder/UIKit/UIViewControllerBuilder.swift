//
//  UIViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 01.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class UIViewControllerBuilder
{
    required init(viewController: UIViewController, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.configuration = configuration
    }
    
    var viewController: UIViewController
    
    var configuration: UIConfiguration
    
    func construct()
    {
        self.viewController.automaticallyAdjustsScrollViewInsets = self.configuration.automaticallyAdjustsScrollViewInsets
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func result() -> UIViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        self.viewController.automaticallyAdjustsScrollViewInsets = true
    }
}
