//
//  TableViewLightTableViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 04.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class TableViewLightTableViewControllerBuilder
{
    required init(viewController: TableViewLightTableViewController, tableView: UITableView, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.tableView = tableView
        
        self.configuration = configuration
    }
    
    var viewController: TableViewLightTableViewController
    
    var configuration: UIConfiguration
    
    var tableView: UITableView
    
    func construct(viewController: UIViewController)
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationItem: UINavigationItem)
    {
        let builder = KeyboardManagementNavigationItemBuilder.init(target: self.viewController)
        builder.construct()
        
        self.viewController.navigationItem.rightBarButtonItems = builder.result()
        
        self.viewController.navigationItem.title = "TableViewController Light"
    }
    
    func construct(tableView: UIScrollView)
    {
        let builder = UIScrollViewBuilder(scrollView: tableView, configuration: self.configuration)
        builder.construct()
        
        self.tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
    }
    
    func construct(view: UIView)
    {
        let builer = UIViewBuilder.init(view: view, configuration: self.configuration)
        builer.construct()
    }
    
    func construct()
    {
    }
    
    func result() -> UIScrollView
    {
        return self.tableView
    }
    
    func reset()
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.reset()
        
        if self.viewController.navigationController != nil
        {
            let builder = UINavigationControllerBuilder(navigationController: self.viewController.navigationController!, configuration: self.configuration)
            
            builder.reset()
        }
    }
}
