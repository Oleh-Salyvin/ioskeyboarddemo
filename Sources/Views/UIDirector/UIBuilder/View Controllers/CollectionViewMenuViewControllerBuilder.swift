//
//  CollectionViewMenuViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 11.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class CollectionViewMenuViewControllerBuilder
{
    required init(viewController: CollectionViewMenuViewController, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.configuration = configuration
    }
    
    var viewController: CollectionViewMenuViewController
    
    var configuration: UIConfiguration
    
    func construct()
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func result() -> CollectionViewMenuViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.reset()
    }
}

