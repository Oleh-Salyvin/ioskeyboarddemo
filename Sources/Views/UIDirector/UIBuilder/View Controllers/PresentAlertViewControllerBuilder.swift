//
//  PresentAlertViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 03.05.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class PresentAlertViewControllerBuilder
{
    required init()
    {
        self.viewController = PresentAlertViewController(nibName: nil, bundle: nil)
    }
    
    var viewController: PresentAlertViewController? = nil
    
    func construct()
    {
    }
    
    func result() -> PresentAlertViewController
    {
        return self.viewController!
    }
}
