//
//  CollectionViewLightViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 04.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class CollectionViewLightViewControllerBuilder
{
    required init(viewController: UIViewController, collectionView: UICollectionView, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.collectionView = collectionView
        
        self.configuration = configuration
    }
    
    var viewController: UIViewController
    
    var collectionView: UICollectionView
    
    var configuration: UIConfiguration
    
    func construct()
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(viewController: UIViewController)
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationItem: UINavigationItem)
    {
        let builder = KeyboardManagementNavigationItemBuilder.init(target: self.viewController)
        builder.construct()
        
        self.viewController.navigationItem.rightBarButtonItems = builder.result()
    }
    
    func construct(collectionView: UICollectionView)
    {
        let builder = UIScrollViewBuilder(scrollView: collectionView, configuration: self.configuration)
        
        builder.construct()
    }
    
    func construct(view: UIView)
    {
        let builer = UIViewBuilder.init(view: view, configuration: self.configuration)
        builer.construct()
    }
    
    func result() -> UICollectionView
    {
        return self.collectionView
    }
    
    func reset()
    {
        if self.viewController.navigationController != nil
        {
            let builder = UINavigationControllerBuilder(navigationController: self.viewController.navigationController!, configuration: self.configuration)
            
            builder.reset()
        }
    }
}
