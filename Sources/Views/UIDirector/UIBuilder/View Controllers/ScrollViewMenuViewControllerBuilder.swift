//
//  ScrollViewMenuViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 03.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class ScrollViewMenuViewControllerBuilder
{
    required init(viewController: ScrollViewMenuViewController, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.configuration = configuration
    }
    
    var viewController: ScrollViewMenuViewController
    
    var configuration: UIConfiguration
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct()
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func result() -> ScrollViewMenuViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.reset()
    }
}
