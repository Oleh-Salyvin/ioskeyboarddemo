//
//  PersonsViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 03.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class PersonsViewControllerBuilder
{
    required init(context: PersonGroupContext)
    {
        self.personGroupContext = context
        
        self.viewController = PersonsViewController(nibName: nil, bundle: nil)
    }
    
    var personGroupContext: PersonGroupContext
    
    var viewController: PersonsViewController?
    
    func construct()
    {
        self.viewController?.personGroupContext = self.personGroupContext
    }
    
    func result() -> PersonsViewController
    {
        return self.viewController!
    }
}
