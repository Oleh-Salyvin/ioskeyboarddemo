//
//  ScrollViewLightViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 03.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


final class ScrollViewLightViewControllerBuilder
{
    required init(_ viewController: ScrollViewLightViewController, scrollView: UIScrollView, viewContext: ScrollViewLightViewContext, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.scrollView = scrollView
        
        self.viewContext = viewContext
        
        self.configuration = configuration
    }
    
    var viewController: ScrollViewLightViewController
    
    var scrollView: UIScrollView
    
    var viewContext: ScrollViewLightViewContext
    
    var configuration: UIConfiguration
    
    func construct()
    {
        self.viewController.navigationItem.title = self.viewContext.navigationItemTitle()
    }
    
    func construct(viewController: UIViewController)
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationItem: UINavigationItem)
    {
        let builder = KeyboardManagementNavigationItemBuilder(target: self.viewController)
        builder.construct()
        
        self.viewController.navigationItem.rightBarButtonItems = builder.result()
    }
    
    func construct(scrollView: UIScrollView)
    {
        let builder = UIScrollViewBuilder(scrollView: scrollView, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(view: UIView)
    {
        let builer = UIViewBuilder(view: view, configuration: self.configuration)
        builer.construct()
    }
    
    func result() -> ScrollViewLightViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        let vcBuilder = UIViewControllerBuilder(viewController: self.viewController, configuration: UIConfiguration())
        vcBuilder.reset()
        
        let svBuilder = UIScrollViewBuilder(scrollView: self.scrollView, configuration: UIConfiguration())
        svBuilder.reset()
        
        if let _navCon = self.viewController.navigationController
        {
            let builder = UINavigationControllerBuilder(navigationController: _navCon, configuration: UIConfiguration())
            builder.reset()
        }
    }
}
