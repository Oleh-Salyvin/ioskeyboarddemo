//
//  ScrollViewLightMixViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 12.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class ScrollViewLightMixViewControllerBuilder
{
    required init(_ viewController: ScrollViewLightMixViewController, scrollView: UIScrollView, viewModel: ScrollViewLightMixViewContext, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.viewModel = viewModel
        
        self.scrollView = scrollView
        
        self.configuration = configuration
    }
    
    var viewController: ScrollViewLightMixViewController
    
    var scrollView: UIScrollView
    
    var viewModel: ScrollViewLightMixViewContext
    
    var configuration: UIConfiguration
    
    func construct(viewController: UIViewController)
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationItem: UINavigationItem)
    {
        let builder = KeyboardManagementNavigationItemBuilder(target: self.viewController)
        builder.construct()
        
        self.viewController.navigationItem.rightBarButtonItems = builder.result()
    }
    
    func construct(scrollView: UIScrollView)
    {
        let builder = UIScrollViewBuilder(scrollView: scrollView, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(view: UIView)
    {
        let builer = UIViewBuilder(view: view, configuration: self.configuration)
        builer.construct()
    }
    
    func construct()
    {
        self.viewController.navigationItem.title = self.viewModel.navigationItemTitle()
        self.viewController.navigationItem.prompt = self.viewModel.navigationItemTitle()
    }
    
    func result() -> ScrollViewLightViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        let vcBuilder = UIViewControllerBuilder(viewController: self.viewController, configuration: UIConfiguration())
        vcBuilder.reset()
        
        let svBuilder = UIScrollViewBuilder(scrollView: self.scrollView, configuration: UIConfiguration())
        svBuilder.reset()
        
        if let _navCon = self.viewController.navigationController
        {
            let builder = UINavigationControllerBuilder(navigationController: _navCon, configuration: UIConfiguration())
            builder.reset()
        }
    }
}
