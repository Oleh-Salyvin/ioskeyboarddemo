//
//  ScrollViewHardMixViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 03.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class ScrollViewHardMixViewControllerBuilder
{
    required init(viewController: ScrollViewHardMixViewController, scrollView: UIScrollView, contentView: UIView, viewContext: ScrollViewHardMixViewContext, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.scrollView = scrollView
        
        self.contentView = contentView
        
        self.viewContext = viewContext
        
        self.configuration = configuration
    }
    
    var viewController: ScrollViewHardMixViewController
    
    var scrollView: UIScrollView
    
    var contentView: UIView
    
    var configuration: UIConfiguration
    
    var viewContext: ScrollViewHardMixViewContext
    
    private var contentViewBottomConstraint: NSLayoutConstraint? = nil
    
    func construct(viewController: UIViewController)
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationItem: UINavigationItem)
    {
        let builder = KeyboardManagementNavigationItemBuilder.init(target: self.viewController)
        builder.construct()
        
        self.viewController.navigationItem.rightBarButtonItems = builder.result()
    }
    
    func construct(view: UIView)
    {
        let builer = UIViewBuilder.init(view: view, configuration: self.configuration)
        builer.construct()
    }
    
    func construct(scrollView: UIScrollView)
    {
        let builder = UIScrollViewBuilder(scrollView: scrollView, configuration: self.configuration)
        builder.construct()
    }
    
    func construct()
    {
        self.viewController.navigationItem.title = self.viewContext.navigationItemTitle()
        
        let componentSize = 5
        let amountOfComponents = 1
        var index: Int = 1
        
        for raw in 1...componentSize * amountOfComponents
        {
            if index == 1
            {
                self.construct(context: self.viewContext.personGroupContext)
            }
            else if index == 2
            {
                self.construct(textField: self.viewContext.textFieldViewContext, index: raw)
            }
            else if index == 3
            {
                self.construct(presentAlertViewController: raw)
            }
            else if index == 4
            {
                self.construct(context: self.viewContext.personGroupContext, number: 1, offset: raw)
            }
            else if index == 5
            {
                self.construct(label: self.viewContext.labelViewContext, index: raw)
                
                index = 1
                
                continue
            }
            
            index += 1
        }
    }
    
    func construct(context: PersonGroupContext, number: Int, offset offsetIndex: Int)
    {
        if let _persons = context.person(number: 1, offset: offsetIndex), _persons.isEmpty == false
        {
            let builder = PersonViewControllerBuilder(personEntity: _persons.first!)
            builder.construct(index: offsetIndex)
            
            let vc = builder.result()
            
            vc.keyboardManagement = self.viewController.keyboardManagement
            
            self.viewController.addChild(vc)
            
            self.insert(view: vc.view, in: self.scrollView.subviews.first!)
        }
    }
    
    func construct(textField textFieldViewModel: TextFieldViewContext, index: Int)
    {
        let builder = TextFieldViewControllerBuilder(viewModel: textFieldViewModel)
        builder.construct(index: index)
        
        let vc = builder.result()
        
        vc.keyboardManagement = self.viewController.keyboardManagement
        
        self.viewController.addChild(vc)
        
        self.insert(view: vc.view, in: self.scrollView.subviews.first!)
    }
    
    func construct(label labelViewModel: LabelViewContext, index: Int)
    {
        let builder = LabelViewControllerBuilder(viewModel: self.viewContext.labelViewContext)
        builder.construct(index: index)
        
        let vc = builder.result()
        self.viewController.addChild(vc)
        
        self.insert(view: vc.view, in: self.scrollView.subviews.first!)
    }
    
    func construct(context: PersonGroupContext)
    {
        let builder = PersonsViewControllerBuilder(context: context)
        builder.construct()
        
        let vc = builder.result()
        
        vc.keyboardManagement = self.viewController.keyboardManagement
        
        self.viewController.addChild(vc)
        
        self.insert(view: vc.view, in: self.scrollView.subviews.first!)
    }
    
    func construct(presentAlertViewController index: Int)
    {
        let builder = PresentAlertViewControllerBuilder()
        builder.construct()
        
        let vc = builder.result()
        self.viewController.addChild(vc)
        
        self.insert(view: vc.view, in: self.scrollView.subviews.first!)
    }
    
    func result() -> ScrollViewHardMixViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        let vcBuilder = UIViewControllerBuilder(viewController: self.viewController, configuration: UIConfiguration())
        vcBuilder.reset()
        
        let svBuilder = UIScrollViewBuilder(scrollView: self.scrollView, configuration: UIConfiguration())
        svBuilder.reset()
        
        if let _navCon = self.viewController.navigationController
        {
            let builder = UINavigationControllerBuilder(navigationController: _navCon, configuration: UIConfiguration())
            builder.reset()
        }
    }
    
    private func insert(view: UIView, in contentView: UIView)
    {
        if (contentView.subviews.isEmpty == true)
        {
            contentView.addSubview(view)
            
            self.contentViewBottomConstraint = NSLayoutConstraint(item: contentView, attribute: .bottomMargin, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
            
            contentView.addConstraints([
                NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leadingMargin, multiplier: 1, constant: 0),
                NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailingMargin, multiplier: 1, constant: 0),
                
                NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .topMargin, multiplier: 1, constant: 0),
                self.contentViewBottomConstraint!
                ])
        }
        else
        {
            self.contentViewBottomConstraint?.isActive = false
            
            let attachToView = contentView.subviews.last!
            contentView.insertSubview(view, aboveSubview: attachToView)
            
            self.contentViewBottomConstraint = NSLayoutConstraint(item: contentView, attribute: .bottomMargin, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
            
            contentView.addConstraints([
                NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leadingMargin, multiplier: 1, constant: 0),
                NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailingMargin, multiplier: 1, constant: 0),
                
                NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: attachToView, attribute: .bottom, multiplier: 1, constant: 0),
                self.contentViewBottomConstraint!
                ])
        }
    }
}
