//
//  ScrollViewLongContentSizeViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 22.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


final class ScrollViewLongContentSizeViewControllerBuilder
{
    required init(_ viewController: ScrollViewLongContentSizeViewController, scrollView: UIScrollView, viewModel: ScrollViewLongContentSizeViewContext, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.scrollView = scrollView
        
        self.viewModel = viewModel
        
        self.configuration = configuration
    }
    
    var viewController: ScrollViewLongContentSizeViewController
    
    var scrollView: UIScrollView
    
    var viewModel: ScrollViewLongContentSizeViewContext
    
    var configuration: UIConfiguration
    
    func construct()
    {
        self.viewController.navigationItem.title = self.viewModel.navigationItemTitle()
    }
    
    func construct(viewController: UIViewController)
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationItem: UINavigationItem)
    {
        let builder = KeyboardManagementNavigationItemBuilder(target: self.viewController)
        builder.construct()
        
        self.viewController.navigationItem.rightBarButtonItems = builder.result()
    }
    
    func construct(scrollView: UIScrollView)
    {
        let builder = UIScrollViewBuilder(scrollView: scrollView, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(view: UIView)
    {
        let builer = UIViewBuilder(view: view, configuration: self.configuration)
        builer.construct()
    }
    
    func result() -> ScrollViewLongContentSizeViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        let vcBuilder = UIViewControllerBuilder(viewController: self.viewController, configuration: UIConfiguration())
        vcBuilder.reset()
        
        let svBuilder = UIScrollViewBuilder(scrollView: self.scrollView, configuration: UIConfiguration())
        svBuilder.reset()
        
        if let _navCon = self.viewController.navigationController
        {
            let builder = UINavigationControllerBuilder(navigationController: _navCon, configuration: UIConfiguration())
            builder.reset()
        }
    }
}

