//
//  TableViewLightViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 04.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class TableViewLightViewControllerBuilder
{
    required init(viewController: TableViewLightViewController, tableView: UITableView, viewContext: TableViewLightViewContext, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.tableView = tableView
        
        self.viewContext = viewContext
        
        self.configuration = configuration
    }
    
    var viewController: TableViewLightViewController
    
    var tableView: UITableView
    
    var configuration: UIConfiguration
    
    var viewContext: TableViewLightViewContext
    
    func construct()
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
        
        self.viewController.navigationItem.title = self.viewContext.navigationItemTitle
    }
    
    func construct(viewController: UIViewController)
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationItem: UINavigationItem)
    {
        let builder = KeyboardManagementNavigationItemBuilder(target: self.viewController)
        builder.construct()
        
        self.viewController.navigationItem.rightBarButtonItems = builder.result()
    }
    
    func construct(view: UIView)
    {
        let builer = UIViewBuilder.init(view: view, configuration: self.configuration)
        builer.construct()
    }
    
    func construct(tableView: UIScrollView)
    {
        let builder = UIScrollViewBuilder(scrollView: tableView, configuration: self.configuration)
        builder.construct()
        
        self.tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        self.tableView.register(UINib(nibName: "TextViewCell", bundle: nil), forCellReuseIdentifier: "TextViewCell")
    }
    
    func result() -> TableViewLightViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        if self.viewController.navigationController != nil
        {
            let builder = UINavigationControllerBuilder(navigationController: self.viewController.navigationController!, configuration: self.configuration)
            
            builder.reset()
        }
    }
}
