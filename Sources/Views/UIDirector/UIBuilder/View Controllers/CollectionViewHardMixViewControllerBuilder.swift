//
//  CollectionViewHardMixViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 04.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class CollectionViewHardMixViewControllerBuilder
{
    required init(viewController: CollectionViewHardMixViewController, collectionView: UICollectionView, configuration: UIConfiguration)
    {
        self.viewController = viewController
        
        self.collectionView = collectionView
        
        self.configuration = configuration
    }
    
    var viewController: CollectionViewHardMixViewController
    
    var collectionView: UICollectionView
    
    var configuration: UIConfiguration
    
    func construct(viewController: UIViewController)
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationController: UINavigationController)
    {
        let builder = UINavigationControllerBuilder(navigationController: navigationController, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(navigationItem: UINavigationItem)
    {
        let builder = KeyboardManagementNavigationItemBuilder.init(target: self.viewController)
        builder.construct()
        
        self.viewController.navigationItem.rightBarButtonItems = builder.result()
    }
    
    func construct(collectionView: UICollectionView)
    {
        let builder = UIScrollViewBuilder(scrollView: collectionView, configuration: self.configuration)
        builder.construct()
    }
    
    func construct(view: UIView)
    {
        let builder = UIViewBuilder.init(view: view, configuration: self.configuration)
        builder.construct()
    }
    
    func construct()
    {
    }
    
    func result() -> CollectionViewHardMixViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        let builder = UIViewControllerBuilder(viewController: self.viewController, configuration: self.configuration)
        builder.reset()
        
        if let _navCon = self.viewController.navigationController
        {
            let builder = UINavigationControllerBuilder(navigationController: _navCon, configuration: self.configuration)
            builder.reset()
        }
    }
}
