//
//  TextFieldCellBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 11.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class TextFieldCellBuilder
{
    required init(tableView: UITableView, viewModel: TableViewLightViewContext, for indexPath: IndexPath, configuration: UIConfiguration)
    {
        self.tableView = tableView
        
        self.viewModel = viewModel
        
        self.indexPath = indexPath
        
        self.cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
    }
    
    var tableView: UITableView
    
    var viewModel: TableViewLightViewContext
    
    var cell: TextFieldCell
    
    var indexPath: IndexPath
    
    func construct()
    {
        cell.textField.placeholder = self.viewModel.textFieldPlaceholder(at: indexPath)
    }
    
    func result() -> TextFieldCell
    {
        return self.cell
    }
    
    func reset()
    {
        self.cell.textField.text = nil
        self.cell.textField.placeholder = nil
    }
}
