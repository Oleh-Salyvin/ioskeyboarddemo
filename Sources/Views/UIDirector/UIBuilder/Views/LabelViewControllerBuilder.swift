//
//  LabelViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 03.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class LabelViewControllerBuilder
{
    required init(viewModel: LabelViewContext)
    {
        self.viewModel = viewModel
        
        self.viewController = LabelViewController(nibName: nil, bundle: nil)
    }
    
    var viewModel: LabelViewContext
    
    var viewController: LabelViewController
    
    func construct(index: Int)
    {
        self.viewController.loadViewIfNeeded()
        
        self.viewController.timeLabel.text = self.viewModel.text(index: index)
    }
    
    func result() -> LabelViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        self.viewController.timeLabel.text = nil
    }
}
