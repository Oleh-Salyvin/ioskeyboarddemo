//
//  KeyboardManagementToolBarBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 02.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class KeyboardManagementToolBarBuilder
{
    required init(_ view: UIView, target: AnyObject)
    {
        self.view = view
        
        self.target = target
    }
    
    var view: UIView
    
    var target: AnyObject
    
    var toolbar: UIToolbar? = nil
    
    func construct()
    {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 50))
        toolbar.barStyle = .default
        
        let onBackSelector = #selector(AnyObject.onPreviousFirstResponder)
        let onHideSelector = #selector(AnyObject.onHideKeyboard)
        let onNextSelector = #selector(AnyObject.onNextFirstResponder)
        
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "<", style: .done, target: self.target, action: onBackSelector),
            UIBarButtonItem(title: "Hide", style: .done, target: self.target, action: onHideSelector),
            UIBarButtonItem(title: ">", style: .done, target: self.target, action: onNextSelector)
        ]
        
        self.toolbar = toolbar
    }
    
    func result() -> UIToolbar?
    {
        return self.toolbar
    }
    
    func reset()
    {
        self.toolbar = nil
    }
}
