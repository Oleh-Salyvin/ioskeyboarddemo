//
//  TextFieldViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 03.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class TextFieldViewControllerBuilder
{
    required init(viewModel: TextFieldViewContext)
    {
        self.viewModel = viewModel
        
        self.viewController = TextFieldViewController(nibName: nil, bundle: nil)
    }
    
    var viewModel: TextFieldViewContext
    
    var viewController: TextFieldViewController
    
    func construct(index: Int)
    {
        self.viewController.loadViewIfNeeded()
        
        self.viewController.textField.text = self.viewModel.text(index: index)
        self.viewController.textField.delegate = self.viewController
    }
    
    func result() -> TextFieldViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        self.viewController.textField.text = nil
    }
}
