//
//  TextViewCellBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 11.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class TextViewCellBuilder
{
    required init(tableView: UITableView, viewModel: TableViewLightViewContext, for indexPath: IndexPath, configuration: UIConfiguration)
    {
        self.tableView = tableView
        
        self.viewModel = viewModel
        
        self.indexPath = indexPath
        
        self.cell = tableView.dequeueReusableCell(withIdentifier: "TextViewCell", for: indexPath) as! TextViewCell
    }
    
    var tableView: UITableView
    
    var viewModel: TableViewLightViewContext
    
    var cell: TextViewCell
    
    var indexPath: IndexPath
    
    func construct()
    {
        self.cell.textView.text = self.viewModel.textViewText(at: self.indexPath)
    }
    
    func result() -> TextViewCell
    {
        return self.cell
    }
    
    func reset()
    {
        self.cell.textView.text = nil
    }
}
