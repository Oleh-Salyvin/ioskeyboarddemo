//
//  PersonViewControllerBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 03.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class PersonViewControllerBuilder
{
    required init(personEntity: PersonEntity)
    {
        self.personEntity = personEntity
        
        self.viewController = PersonViewController(nibName: nil, bundle: nil)
    }
    
    let personEntity: PersonEntity
    
    var viewController: PersonViewController
    
    func construct(index: Int)
    {
        self.viewController.personEntity = self.personEntity
        
        self.viewController.firstNameTextField.text = self.personEntity.firstName
        
        self.viewController.lastNameTextField.text = self.personEntity.lastName
        
        self.viewController.ageTextField.text = self.personEntity.age
    }
    
    func result() -> PersonViewController
    {
        return self.viewController
    }
    
    func reset()
    {
        self.viewController.firstNameTextField.text = nil
        
        self.viewController.lastNameTextField.text = nil
        
        self.viewController.ageTextField.text = nil
    }
}
