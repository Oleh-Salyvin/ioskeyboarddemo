//
//  KeyboardManagementNavigationItemBuilder.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 02.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class KeyboardManagementNavigationItemBuilder
{
    required init(target: AnyObject)
    {
        self.target = target
    }
    
    var target: AnyObject
    
    var navigationItems: Array<UIBarButtonItem>? = nil
    
    func construct()
    {
        let onBackSelector = #selector(AnyObject.onPreviousFirstResponder)
        let onHideSelector = #selector(AnyObject.onHideKeyboard)
        let onNextSelector = #selector(AnyObject.onNextFirstResponder)
        
        self.navigationItems = [
            UIBarButtonItem(title: ">", style: .done, target: self.target, action: onNextSelector),
            UIBarButtonItem(title: "Hide", style: .done, target: self.target, action: onHideSelector),
            UIBarButtonItem(title: "<", style: .done, target: self.target, action: onBackSelector)
        ]
    }
    
    func result() -> Array<UIBarButtonItem>?
    {
        return self.navigationItems
    }
    
    func reset()
    {
        self.navigationItems = nil
    }
}
