//
//  CollectionViewMenuViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 05.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class CollectionViewMenuViewController: PresenterViewController
{
    @IBOutlet weak var collectionViewLightButton: UIButton!
    @IBOutlet weak var collectionViewHardMixButton: UIButton!
    
    var destinationConfiguration: UIConfiguration? = nil
    
    override func settings() -> UIConfiguration
    {
        if (self.activeConfiguration == nil)
        {
            let configuration = UIConfiguration()
            
            self.activeConfiguration = configuration
        }
        return self.activeConfiguration!
    }
    
    override func viewDidLoad()
    {
        self.keyboardManagement = nil
        
        super.viewDidLoad()
        
        let builder = CollectionViewMenuViewControllerBuilder(viewController: self, configuration: self.settings())
        self.uiDirector.constructCollectionViewMenuViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let builder = CollectionViewMenuViewControllerBuilder(viewController: self, configuration: self.settings())
        self.uiDirector.constructCollectionViewMenuViewController(builder, workType: .configure)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if var _protocol = segue.destination as? SettingsActiveConfigurationClientProtocol
        {
            _protocol.activeConfiguration = self.destinationConfiguration
            self.destinationConfiguration = nil
        }
    }
    
    @IBAction func onLongPressShowConfiguration(_ sender: UILongPressGestureRecognizer)
    {
        var segueIdentifier: String = String()
        
        if (sender.view == self.collectionViewLightButton)
        {
            segueIdentifier = "showCollectionViewLight"
        }
        else if (sender.view == self.collectionViewHardMixButton)
        {
            segueIdentifier = "showCollectionViewHardMix"
        }
        
        if (sender.state == .began)
        {
            let vc = SettingsViewController(nibName: nil, bundle: nil)
            vc.onHideClosure = { [weak self] in
                self?.dismiss(animated: true, completion: {
                    
                })
            }
            vc.onShowClosure = { [weak self] (_ configuration: UIConfiguration) in
                self?.destinationConfiguration = configuration
                
                self?.dismiss(animated: true, completion: { [weak self] in
                    
                    
                    self?.performSegue(withIdentifier: segueIdentifier, sender: vc)
                })
            }
            
            self.present(vc, animated: true, completion: {
                
            })
        }
    }
}
