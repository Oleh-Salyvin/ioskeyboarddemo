//
//  CollectionViewCell.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 6/19/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class CollectionViewCell: UICollectionViewCell, KMVisitorComponentViewProtocol
{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
        
        self.textField.placeholder = nil
        self.textField.text = nil
    }
    
    //MARK: - KMVisitorComponentViewProtocol
    
    func accept(visitor: inout KMViewVisitorProtocol)
    {
        visitor.visit(view: self, firstResponders: [self.textField])
    }
}
