//
//  CollectionViewCompactContext.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 6/19/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class CollectionViewCompactContext: NSObject, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    required init(collectionView: UICollectionView)
    {
        super.init()
        
        self.collectionView = collectionView
        self.collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
    }
    
    var active: Bool = false
    
    weak var viewController: CollectionViewHardMixViewController?
    
    private weak var collectionView: UICollectionView!
    
    private let layout = CollectionViewCompactLayout()
    
    private var recipes: Array<Recipe?> = Recipe.fetchCompact(count: 20)
    
    private var cellSized: CollectionViewCell?
    
    func setCollectionViewLayout()
    {
        self.active = true
        self.collectionView.backgroundColor = UIColor.blue
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.collectionView.setCollectionViewLayout(self.layout, animated: true) { (flag) in
            print("\(self)")
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.recipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        
        let recipe = self.recipes[indexPath.row]
        
        if (indexPath.row % 2 == 0)
        {
            cell.textField.keyboardType = .emailAddress
        }
        else
        {
            cell.textField.keyboardType = .default
        }
        
        cell.nameLabel.text =  recipe?.name
        cell.textLabel.text = "\(indexPath.row) " + (recipe?.text)!
        cell.textField.placeholder = recipe?.editableText
        cell.textField.delegate = self.viewController
        
        return cell
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        print("\(#function)")
        
        if (self.cellSized == nil)
        {
            self.cellSized = Bundle.main.loadNibNamed("CollectionViewCell", owner: self, options: nil)?.first as? CollectionViewCell
        }
        
        guard let cell = self.cellSized else {
            fatalError()
        }
        
        let recipe = self.recipes[indexPath.row]
        
        cell.nameLabel.text = recipe?.name
        cell.textLabel.text = recipe?.text
        
        cell.bounds.size.width = self.collectionView.contentSize.width - self.layout.sectionInset.left - self.layout.sectionInset.right - self.layout.minimumInteritemSpacing - self.collectionView.contentInset.left - self.collectionView.contentInset.right
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        let viewWithTag = cell.contentView.viewWithTag(-1)
        
        guard let _viewWithTag = viewWithTag, viewWithTag != nil else {
            fatalError()
        }
        
        cell.bounds.size.height = _viewWithTag.bounds.size.height
        
        return cell.bounds.size
    }
    /*
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return self.layout.sectionInset
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return self.layout.sectionInset.left
    }*/
}
