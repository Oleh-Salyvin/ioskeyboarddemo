//
//  Recipe.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 31.07.17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import Foundation


class Recipe
{
    var name: String?
    var text: String?
    var fullText: String?
    var editableText: String?
    
    init(name: String, text: String?, fText: String?, eText: String?)
    {
        setup(name: name, text: text, fText: fText, eText: eText)
    }
    
    func setup(name: String, text: String?, fText: String?, eText: String?)
    {
        self.name = name
        self.text = text
        self.fullText = fText
        self.editableText = eText
    }
}


extension Recipe
{
    class func fetchCompact(count: Int) -> Array<Recipe?>
    {
        var data = [Recipe?]()
        
        for _ in 0..<count
        {
            data.append(Recipe(name: "Compact", text: "Line 1", fText: nil, eText: "Editable text"))
        }
        
        return data
    }
    
    class func fetchDetailed(count: Int) -> Array<Recipe?>
    {
        var data = [Recipe?]()
        
        for _ in 0..<count
        {
            data.append(Recipe(name: "Detailed", text: nil, fText: "Line 1\nLine 2", eText: "Editable text"))
        }
        
        data.insert(Recipe(name: "Detailed Long", text: nil, fText: "Line 1\nLine 2\nLine 3\nLine 4\nLine 5", eText: "Editable text"), at: 2)
        
        return data
    }
}
