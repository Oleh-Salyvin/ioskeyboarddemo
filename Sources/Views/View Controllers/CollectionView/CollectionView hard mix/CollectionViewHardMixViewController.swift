//
//  CollectionViewHardMixViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 6/19/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class CollectionViewHardMixViewController: WarmCollectionViewController
{
    private var compactController: CollectionViewCompactContext?
    
    private var detailedController: CollectionViewDetailedContext?
    
    private var keyboardManagementContext = KeyboardManagementContext()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let configuration = self.configuration()
        
        self.keyboardManagement = KeyboardManagement()
        
        if self.keyboardManagement != nil
        {
            let attributedAlignment = configuration.attributedAlignment
            attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenter, .visibleRect))
            self.keyboardManagementContext.attributedAlignment = attributedAlignment
            
            self.keyboardManagement!.scrollView = self.collectionView
            self.keyboardManagement!.notificationDelegate = self.keyboardManagementContext
            self.keyboardManagement!.isEnabled = true
            self.keyboardManagement!.isPipelineEnabled = true
        }
        
        super.configure(scrollView: self.collectionView, configuration: configuration)
        
        let builder = CollectionViewHardMixViewControllerBuilder(viewController: self, collectionView: self.collectionView!, configuration: configuration)
        self.uiDirector.constructCollectionViewHardMixViewController(builder, workType: .construct)
        
        self.navigationItem.title = "CollectionView";
        
        self.compactController = CollectionViewCompactContext(collectionView: self.collectionView)
        self.detailedController = CollectionViewDetailedContext(collectionView: self.collectionView)
        
        precondition(self.compactController != nil)
        precondition(self.detailedController != nil)
        
        self.selectLayout(index: 0)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.configuration()
        
        let builder = CollectionViewHardMixViewControllerBuilder(viewController: self, collectionView: self.collectionView!, configuration: configuration)
        self.uiDirector.constructCollectionViewHardMixViewController(builder, workType: .configure)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?)
    {
        self.collectionView.performBatchUpdates({
        }) { (flag) in
        }
    }
    
    @IBAction func segmentedControlLayout(_ sender: UISegmentedControl)
    {
        self.selectLayout(index: sender.selectedSegmentIndex)
    }
    
    private func selectLayout(index: Int)
    {
        self.detailedController?.active = false
        self.compactController?.active = false
        
        if (index == 0)
        {
            self.compactController?.viewController = self
            self.compactController?.setCollectionViewLayout()
        }
        else
        {
            self.detailedController?.viewController = self
            self.detailedController?.setCollectionViewLayout()
        }
    }
}
