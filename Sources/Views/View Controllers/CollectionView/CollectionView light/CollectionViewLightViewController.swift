//
//  CollectionViewLightViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 22.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class CollectionViewLightViewController: CollectionViewController, CollectionViewLightViewLayoutDelegate, CollectionViewCellColorDelegate
{
    private var layout: CollectionViewLightViewLayout {
        return (self.collectionViewLayout as! CollectionViewLightViewLayout)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let configuration = self.configuration()
        
        self.keyboardManagement = KeyboardManagement()
        
        if self.keyboardManagement != nil
        {
            self.keyboardManagement!.scrollView = self.collectionView
            self.keyboardManagement!.isEnabled = true
        }
        
        super.configure(scrollView: self.collectionView, configuration: configuration)
        
        self.layout.delegate = self
        
        let builder = CollectionViewLightViewControllerBuilder(viewController: self, collectionView: self.collectionView!, configuration: self.settings())
        self.uiDirector.constructCollectionViewLightViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.configuration()
        
        let builder = CollectionViewLightViewControllerBuilder(viewController: self, collectionView: self.collectionView!, configuration: configuration)
        self.uiDirector.constructCollectionViewLightViewController(builder, workType: .configure)
    }
    
    //MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 25
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewColorCell", for: indexPath) as! CollectionViewColorCell
        cell.textField.placeholder = "\(indexPath.row)"
        cell.textField.delegate = self
        cell.delegate = self
        return cell
    }
    
    //MARK: - CollectionViewCellColorDelegate
    
    func nextFirstResponder(cell: CollectionViewColorCell) -> Bool
    {
        return self.keyboardManagement?.next() != nil ? true : false
    }
    
    //MARK: - UICollectionViewVerticalLayoutDelegate
    
    func collectionView(_ collectionView: UICollectionView, heightForCellAtIndexPath: IndexPath, withWidth: CGFloat) -> CGFloat
    {
        if heightForCellAtIndexPath.row == 0
        {
            return 250
        }
        else if heightForCellAtIndexPath.row == 1
        {
            return 200
        }
        return 150
    }
    
    //MARK: - SettingsActiveConfigurationClientProtocol
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        if #available(iOS 11.0, *)
        {}
        else
        {
            configuration.automaticallyAdjustsScrollViewInsets = true
        }
        
        return configuration
    }
}
