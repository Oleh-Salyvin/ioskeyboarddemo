//
//  CollectionViewColorCell.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 29.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class CollectionViewColorCell: UICollectionViewCell, UITextFieldDelegate, KMVisitorComponentViewProtocol
{
    @IBOutlet weak var textField: UITextField!
    
    weak var delegate: CollectionViewCellColorDelegate?
    
    //MARK: - KMVisitorComponentViewProtocol
    
    func accept(visitor: inout KMViewVisitorProtocol)
    {
        visitor.visit(view: self, firstResponders: [self.textField])
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return delegate!.nextFirstResponder(cell: self)
    }
}
