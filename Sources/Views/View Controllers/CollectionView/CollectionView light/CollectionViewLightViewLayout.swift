//
//  CollectionViewLightViewLayout.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 22.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


protocol CollectionViewLightViewLayoutDelegate: class
{
    func collectionView(_ collectionView: UICollectionView, heightForCellAtIndexPath: IndexPath, withWidth: CGFloat) -> CGFloat
}


class CollectionViewLightViewLayout: UICollectionViewLayout
{
    weak var delegate: CollectionViewLightViewLayoutDelegate?
    
    var numberOfColumns = 2
    
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    
    fileprivate var contentHeight: CGFloat = 0
    
    fileprivate var contentWidth: CGFloat {
        let insets = collectionView!.contentInset
        return collectionView!.bounds.width - (insets.left + insets.right)
    }
}


extension CollectionViewLightViewLayout
{
    override func prepare()
    {
        super.prepare()
        
        self.cache = [UICollectionViewLayoutAttributes]()
        
        self.contentHeight = 0
        
        let columnWidth: CGFloat = self.contentWidth / CGFloat(self.numberOfColumns)
        let cellPadding: CGFloat = 8
        
        var xOffset = [CGFloat]()
        
        for column in 0..<self.numberOfColumns
        {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        
        var yOffset = [CGFloat](repeatElement(0, count: self.numberOfColumns))
        
        
        for item in 0..<self.collectionView!.numberOfItems(inSection: 0)
        {
            let indexPath = IndexPath(item: item, section: 0)
            
            let width: CGFloat = columnWidth - cellPadding * 2
            let cellHeight: CGFloat = self.delegate!.collectionView(self.collectionView!, heightForCellAtIndexPath: indexPath, withWidth: width)
            let height: CGFloat = cellHeight + cellPadding * 2
            
            var shortestColumn = 0
            
            if let minYOffset = yOffset.min()
            {
                shortestColumn = yOffset.index(of: minYOffset) ?? 0
            }
            
            let content = CGRect(x: xOffset[shortestColumn], y: yOffset[shortestColumn], width: columnWidth, height: height)
            let insetFrame = content.insetBy(dx: cellPadding, dy: cellPadding)
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            self.cache.append(attributes)
            
            self.contentHeight = max(self.contentHeight, content.maxY)
            
            yOffset[shortestColumn] = yOffset[shortestColumn] + height
        }
    }
}


extension CollectionViewLightViewLayout
{
    override var collectionViewContentSize: CGSize {
        get {
            return CGSize(width: self.contentWidth, height: self.contentHeight)
        }
    }
}


extension CollectionViewLightViewLayout
{
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]?
    {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attributes in cache
        {
            if attributes.frame.intersects(rect)
            {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes?
    {
        return cache.first { attributes -> Bool in
            return attributes.indexPath == indexPath
        }
    }
}
