//
//  Protocols.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 30.01.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


protocol RedirectDelegate: class
{
    func accessRedirected(redirectView: UIView, visitor: inout KMViewVisitorProtocol)
}


protocol CollectionViewCellColorDelegate: class
{
    func nextFirstResponder(cell: CollectionViewColorCell) -> Bool
}


protocol DemoNextResponderDelegate: class
{
    func nextFirstResponder(currentCell: TextFieldCell) -> Bool
}
