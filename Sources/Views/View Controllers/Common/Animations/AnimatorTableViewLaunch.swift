//
//  AnimatorTableViewLaunch.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 5/9/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


class AnimatorTableViewLaunch
{
    class func animate(in tableView: UITableView, complation: (() -> Swift.Void)? = nil)
    {
        let views = tableView.visibleCells
        
        for view in views
        {
            view.transform = CGAffineTransform(translationX: 0, y: tableView.bounds.size.height)
        }
        
        for (index, view) in views.enumerated()
        {
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                view.transform = CGAffineTransform(translationX: 0, y: 0)
            }, completion: nil)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            if complation != nil
            {
                complation!()
            }
        }
    }
}
