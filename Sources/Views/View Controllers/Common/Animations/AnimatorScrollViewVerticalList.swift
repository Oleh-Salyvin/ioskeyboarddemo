//
//  AnimatorScrollViewVerticalList.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 5/9/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


class AnimatorScrollViewVerticalList
{
    class func animate(in scrollView: UIScrollView, contentView: UIView, views: Array<UIView>)
    {
        let offsetMidY: CGFloat = CGFloat(contentView.frame.origin.y) + CGFloat(contentView.bounds.size.height / 2.0)
        let midX = contentView.bounds.size.width / 2.0
        let ratio = CGFloat(contentView.bounds.size.width / 2) / CGFloat(contentView.bounds.size.height / 2.0)
        
        let offsetY = scrollView.contentOffset.y + contentView.frame.origin.y
        
        for view in views
        {
            let viewOffsetY = offsetY + view.center.y
            
            if viewOffsetY > offsetMidY
            {
                let viewOffsetY = viewOffsetY - offsetMidY
                let viewOffsetX: CGFloat = viewOffsetY * ratio + midX
                
                view.center.x = viewOffsetX
            }
            else
            {
                view.center.x = midX
            }
        }
    }
}
