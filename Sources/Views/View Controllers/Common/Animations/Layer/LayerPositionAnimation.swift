//
//  LayerPositionAnimation.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 12.06.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


public final class LayerPositionAnimation: CABasicAnimation, CAAnimationDelegate
{
    var completion: ((Bool) -> Void)?
    
    static public let animationKey = "position.layer.animation"
    
    static let ViewKey = "key.view"
    
    public func setup(fromPoint: CGPoint, toPoint: CGPoint, delay: CFTimeInterval, duration: CFTimeInterval, view: UIView, completion: ((Bool) -> Void)? = nil)
    {
        self.setValue(view, forKey: LayerPositionAnimation.ViewKey)
        self.completion = completion
        
        self.keyPath = "position"
        self.fillMode = .both
        self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        self.fromValue = NSValue.init(cgPoint: fromPoint)
        self.toValue = NSValue.init(cgPoint: toPoint)
        
        if view.layer.presentation() != nil
        {
            self.beginTime = view.layer.presentation()!.convertTime(CACurrentMediaTime() + delay, from: nil)
        }
        else
        {
            self.beginTime = view.layer.convertTime(CACurrentMediaTime() + delay, from: nil)
        }
        
        self.duration = view.layer.convertTime(duration, from: nil)
        
        self.delegate = self
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        view.layer.position = toPoint
        CATransaction.commit()
    }
    
    public func run(forKey key: String? = LayerPositionAnimation.animationKey)
    {
        guard let view = self.value(forKey: LayerPositionAnimation.ViewKey) as? UIView else { return }
        
        view.layer.add(self, forKey: key)
    }
    
    public func animationDidStart(_ anim: CAAnimation)
    {}
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool)
    {
        self.completion?(flag)
    }
    
    func interrupt()
    {
        guard let view = self.value(forKey: LayerPositionAnimation.ViewKey) as? UIView else { return }
        
        self.setValue(nil, forKey: LayerPositionAnimation.ViewKey)
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        view.layer.position = view.layer.presentation()!.position
        view.layer.removeAnimation(forKey: LayerPositionAnimation.animationKey)
        
        CATransaction.commit()
    }
    
    func cancel()
    {
        guard let view = self.value(forKey: LayerPositionAnimation.ViewKey) as? UIView else { return }
        
        self.setValue(nil, forKey: LayerPositionAnimation.ViewKey)
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        view.layer.position = self.fromValue as! CGPoint
        view.layer.removeAnimation(forKey: LayerPositionAnimation.animationKey)
        
        CATransaction.commit()
    }
}
