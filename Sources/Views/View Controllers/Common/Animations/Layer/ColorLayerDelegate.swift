//
//  ColorLayerDelegate.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 18.06.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class ColorLayerDelegate: NSObject, CALayerDelegate
{
    var fromColor: UIColor = UIColor.gray
    
    var toColor: UIColor = UIColor.gray
    
    var duration: CFTimeInterval = 5
    
    var fromPosition: CGPoint = .zero
    
    var toPosition: CGPoint = .zero
    
    private var onOrderIn: Bool = false
    
    public func action(for layer: CALayer, forKey event: String) -> CAAction?
    {
        if event == "onOrderIn"
        {
            self.onOrderIn = true
        }
        else if event == "onOrderOut"
        {
            self.onOrderIn = false
        }
        
        if self.onOrderIn
        {
            return self.search(for: layer, forKey: event)
        }
        
        return nil
    }
    
    private func search(for layer: CALayer, forKey event: String) -> CAAction?
    {
        if event == "backgroundColor"
        {
            let animation = CABasicAnimation(keyPath: "backgroundColor")
            animation.fromValue = self.fromColor.cgColor
            animation.toValue = self.toColor.cgColor
            
            animation.duration = layer.convertTime(self.duration, from: nil)
            
            return animation
        }
        else if event == "position"
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.fromValue = NSValue(cgPoint: self.fromPosition)
            animation.toValue = NSValue(cgPoint: self.toPosition)
            
            animation.beginTime = layer.convertTime(CACurrentMediaTime(), from: nil)
            animation.duration = layer.convertTime(self.duration, from: nil)
            
            animation.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
            
            return animation
        }
        
        return nil
    }
}

