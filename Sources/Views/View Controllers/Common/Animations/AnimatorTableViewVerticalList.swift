//
//  AnimatorTableViewVerticalList.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 5/8/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


class AnimatorTableViewVerticalList
{
    class func animate(in tableView: UITableView)
    {
        let offsetMidY: CGFloat = CGFloat(tableView.contentOffset.y) + CGFloat(tableView.bounds.size.height / 2.0)
        let midX = tableView.bounds.size.width / 2.0
        let ratio = CGFloat(tableView.bounds.size.width / 2) / CGFloat(tableView.bounds.size.height / 2.0)
        
        for view in tableView.visibleCells
        {
            if view.center.y > offsetMidY
            {
                let viewOffsetY = view.center.y - offsetMidY
                let viewOffsetX: CGFloat = viewOffsetY * ratio + midX
                
                view.center.x = viewOffsetX
            }
            else
            {
                view.center.x = midX
            }
        }
    }
    
    class func animate2(in collectionView: UICollectionView)
    {
        let offsetMidY: CGFloat = CGFloat(collectionView.contentOffset.y) + CGFloat(collectionView.bounds.size.height / 2.0)
        let midX = collectionView.bounds.size.width / 2.0
        let ratio = CGFloat(collectionView.bounds.size.width / 2) / CGFloat(collectionView.bounds.size.height / 2.0)
        
        for view in collectionView.visibleCells
        {
            if view.center.y > offsetMidY
            {
                let viewOffsetY = view.center.y - offsetMidY
                let viewOffsetX: CGFloat = viewOffsetY * ratio + midX
                
                view.center.x = viewOffsetX
            }
            else
            {
                view.center.x = midX
            }
        }
    }
}
