//
//  PresenterTransitioning.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 04.04.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class PresenterTransitioning: NSObject, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning
{
    private var backgroundContainerView = UIView()
    
    private enum TransitionOperation
    {
        case presenting
        
        case dismissed
    }
    
    private var transitionOperation: TransitionOperation?
    
    //MARK: - UIViewControllerTransitioningDelegate
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        self.transitionOperation = .presenting
        
        return self
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        self.transitionOperation = .dismissed
        
        return self
    }
    
    //MARK: - UIViewControllerAnimatedTransitioning
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval
    {
        if self.transitionOperation == .presenting
        {
            return 0.3
        }
        return 0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        switch self.transitionOperation {
        case .presenting?:
            self.presentingAnimateTransition(using: transitionContext)
            
        case .dismissed?:
            self.dismissAnimateTransition(using: transitionContext)
            
        case .none:
            break
        }
    }
    
    private func presentingAnimateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        
        let screens: (from: UIViewController, to: UIViewController) = (
            transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!,
            transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        )
        
        
        self.backgroundContainerView.backgroundColor = UIColor.lightGray
        self.backgroundContainerView.isOpaque = false
        self.backgroundContainerView.alpha = 0.09
        self.backgroundContainerView.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundContainerView.autoresizingMask = [UIView.AutoresizingMask.flexibleLeftMargin, UIView.AutoresizingMask.flexibleTopMargin, UIView.AutoresizingMask.flexibleRightMargin, UIView.AutoresizingMask.flexibleBottomMargin, UIView.AutoresizingMask.flexibleHeight, UIView.AutoresizingMask.flexibleWidth]
        
        
        let containerView = transitionContext.containerView
        containerView.addSubview(self.backgroundContainerView)
        containerView.addSubview(screens.from.view)
        containerView.addSubview(screens.to.view)
        
        self.backgroundContainerView.bounds = containerView.bounds
        self.backgroundContainerView.center = containerView.center
        
        screens.to.view.bounds.size = containerView.bounds.size
        screens.to.view.bounds.size.width = containerView.bounds.size.width / 100 * 120
        screens.to.view.bounds.size.height = containerView.bounds.size.height / 100 * 120
        
        screens.to.view.center = containerView.center
        
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, options: [UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.layoutSubviews], animations: {
            
            screens.to.view.bounds.size = containerView.bounds.size
            screens.to.view.bounds.size.height = screens.to.view.bounds.size.height - 200
            screens.to.view.center = containerView.center
            
        }, completion:
            { (flag) in
                let cancelled = transitionContext.transitionWasCancelled
                
                if cancelled == true
                {
                    keyWindow.addSubview(screens.from.view)
                }
                else
                {
                    keyWindow.addSubview(self.backgroundContainerView)
                    keyWindow.addSubview(screens.to.view)
                }
                
                transitionContext.completeTransition(!cancelled)
        })
    }
    
    private func dismissAnimateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        
        let screens: (from: UIViewController, to: UIViewController) = (
            transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!,
            transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        )
        
        let containerView = transitionContext.containerView
        
        containerView.addSubview(screens.from.view)
        containerView.addSubview(screens.to.view)
        
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, options: [UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.layoutSubviews], animations: {
            
            screens.from.view.bounds.size.width = containerView.bounds.size.width / 100 * 130
            screens.from.view.bounds.size.height = containerView.bounds.size.height / 100 * 130
            screens.from.view.center = containerView.center
            
        }, completion:
            { (flag) in
                let cancelled = transitionContext.transitionWasCancelled
                
                if cancelled == true
                {
                    keyWindow.addSubview(screens.from.view)
                }
                else
                {
                    keyWindow.addSubview(self.backgroundContainerView)
                    keyWindow.addSubview(screens.to.view)
                }
                
                transitionContext.completeTransition(!cancelled)
        })
    }
}
