//
//  PresenterViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 27.03.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


protocol PresentableViewController {}


class PresenterViewController: ViewController
{
    private var transitionManager: PresenterTransitioning?
    
    @available(iOS 5.0, *)
    open override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Swift.Void)? = nil)
    {
        if ((viewControllerToPresent is PresentableViewController) == true)
        {
            self.transitionManager = PresenterTransitioning()
            
            viewControllerToPresent.modalPresentationStyle = .custom
            viewControllerToPresent.transitioningDelegate = self.transitionManager
        }
        
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}
