//
//  TextFieldView.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 09.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class TextFieldView: UIView, KMVisitorComponentViewProtocol
{
    @IBOutlet weak var textField: UITextField!
    
    weak var redirectDelegate: RedirectDelegate?
    
    //MARK: - KMVisitorComponentViewProtocol
    
    func accept(visitor: inout KMViewVisitorProtocol)
    {
        self.redirectDelegate?.accessRedirected(redirectView: self, visitor: &visitor)
    }
}
