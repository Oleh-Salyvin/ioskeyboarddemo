//
//  TextFieldViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 08.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class TextFieldViewController: ViewController, RedirectDelegate
{
    @IBOutlet weak var textField: UITextField!
    
    var rootView: TextFieldView {
        return self.view as! TextFieldView
    }
    
    //MARK: - RedirectDelegate
    
    func accessRedirected(redirectView: UIView, visitor: inout KMViewVisitorProtocol)
    {
        visitor.visit(view: redirectView, firstResponders: self.availableResponders())
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.rootView.redirectDelegate = self
    }
    
    @IBAction func onOnOffTextField(_ sender: Any)
    {
        self.textField.isEnabled = !self.textField.isEnabled
    }
    
    func availableResponders() -> Array<UIView>
    {
        var views: Array<UIView> = []
        
        if self.textField.isEnabled
        {
            views = [self.textField]
        }
        
        return views
    }
}
