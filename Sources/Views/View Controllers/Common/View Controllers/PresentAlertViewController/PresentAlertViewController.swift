//
//  PresentAlertViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 03.05.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class PresentAlertViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @IBAction func onAlert(_ sender: Any)
    {
        let vc = UIAlertController(title: "Demo", message: "Presenting Alert", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Hide", style: .cancel, handler: nil)
        
        vc.addAction(ok)
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onAlertWithTextField(_ sender: Any)
    {
        let vc = UIAlertController(title: "Demo", message: "Presenting Alert with UITextField", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Hide", style: .cancel, handler: nil)
        
        vc.addAction(ok)
        vc.addTextField { (textField) in
            
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onActionSheet(_ sender: UIButton)
    {
        let vc = UIAlertController(title: "Demo", message: "Presenting ActionSheet", preferredStyle: .actionSheet)
        let ok = UIAlertAction(title: "Hide", style: .cancel, handler: nil)
        
        vc.addAction(ok)
        
        self.present(vc, animated: true, completion: nil)
    }
}
