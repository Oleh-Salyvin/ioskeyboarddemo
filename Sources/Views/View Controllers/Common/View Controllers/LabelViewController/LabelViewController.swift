//
//  LabelViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 05.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


class LabelViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.onChangeColorAction()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        self.changeColor()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.colorLayer.frame = CGRect(x: 0, y: 0, width: 50, height: self.view.bounds.height)
    }
    
    private func onChangeColorAction()
    {
        self.colorLayer.backgroundColor = UIColor.yellow.cgColor
        
        let transition = CATransition()
        transition.type = .push
        transition.subtype = .fromLeft
        transition.duration = 4
        transition.repeatCount = Float.greatestFiniteMagnitude
        
        self.colorLayer.actions = ["backgroundColor" : transition]
        
        self.view.layer.addSublayer(self.colorLayer)
    }
    
    private func changeColor()
    {
        let red = CGFloat(arc4random()) / CGFloat(INT_MAX)
        let green = CGFloat(arc4random()) / CGFloat(INT_MAX)
        let blue = CGFloat(arc4random()) / CGFloat(INT_MAX)
        
        self.colorLayer.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1).cgColor
    }
    
    private var colorLayer: CALayer = CALayer()
    
    @IBOutlet weak var timeLabel: UILabel!
}
