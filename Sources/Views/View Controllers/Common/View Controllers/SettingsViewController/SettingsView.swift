//
//  SettingsView.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 27.03.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class SettingsView: UIView
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        if #available(iOS 11.0, *)
        {
            self.scrollViewContentInsetsSegmentedControl.isEnabled = true
        }
        else
        {
            self.scrollViewContentInsetsSegmentedControl.isEnabled = false
        }
        
        self.UIState()
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        self.layer.cornerRadius = 7
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.masksToBounds = true
    }
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var automaticallyAdjustsScrollViewInsetsSwitch: UISwitch!
    
    @IBOutlet weak var hidesBottomBarWhenPushedSwitch: UISwitch!
    @IBOutlet weak var hidesBarsOnSwipeSwitch: UISwitch!
    @IBOutlet weak var hidesBarsOnTapSwitch: UISwitch!
    @IBOutlet weak var hidesBarsWhenKeyboardAppearsSwitch: UISwitch!
    @IBOutlet weak var setToolbarHiddenSwitch: UISwitch!
    
    @IBOutlet weak var scrollViewContentInsetsSegmentedControl: UISegmentedControl!
    @IBOutlet weak var keyboardDismissSgmentedControl: UISegmentedControl!
    @IBOutlet weak var scrollViewHighlightBorderSwitch: UISwitch!
    
    @IBOutlet weak var keyboardManagementAlignmentSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var quiescentAreaSwitch: UISwitch!
    @IBOutlet weak var quiescentAreaHeightTextField: UITextField!
    
    @IBOutlet weak var bounceStackView: UIStackView!
    @IBOutlet weak var bounceOnScroll: UISwitch!
    @IBOutlet weak var bounceOnZoom: UISwitch!
    @IBOutlet weak var bounceHorizontally: UISwitch!
    @IBOutlet weak var bounceVertically: UISwitch!
    
    
    @IBAction func onQuiescentAreaSwitch(_ sender: UISwitch)
    {
        self.UIState()
    }
    
    @IBAction func onCancell(_ sender: Any)
    {
        if self.onHideClosure != nil
        {
            self.onHideClosure!()
        }
    }
    
    @IBAction func onOK(_ sender: Any)
    {
        if self.onShowClosure != nil
        {
            self.onShowClosure!(self.createConfiguration())
        }
    }
    
    func UIState()
    {
        self.quiescentAreaHeightTextField.isEnabled = self.quiescentAreaSwitch.isOn
    }
    
    
    var onHideClosure: (() -> Void)?
    
    var onShowClosure: ((_ configuration: UIConfiguration) -> Void)?
    
    func createConfiguration() -> UIConfiguration
    {
        let configuration = UIConfiguration()
        configuration.automaticallyAdjustsScrollViewInsets = self.automaticallyAdjustsScrollViewInsetsSwitch.isOn
        
        configuration.hidesBottomBarWhenPushed = self.hidesBottomBarWhenPushedSwitch.isOn
        configuration.hidesBarsOnSwipe = self.hidesBarsOnSwipeSwitch.isOn
        configuration.hidesBarsOnTap = self.hidesBarsOnTapSwitch.isOn
        configuration.hidesBarsWhenKeyboardAppears = self.hidesBarsWhenKeyboardAppearsSwitch.isOn
        configuration.setToolbarHidden = (hidden: self.setToolbarHiddenSwitch.isOn, animated: true)
        
        // UIScrollView
        configuration.bounceOnScroll = self.bounceOnScroll.isOn
        configuration.bounceOnZoom = self.bounceOnZoom.isOn
        configuration.bounceHorizontally = self.bounceHorizontally.isOn
        configuration.bounceVertically = self.bounceVertically.isOn
        
        if #available(iOS 11.0, *)
        {
            configuration.contentInsetAdjustmentBehavior = ContentInsetAdjustmentBehavior(rawValue: self.scrollViewContentInsetsSegmentedControl.selectedSegmentIndex)!
        }
        
        switch self.keyboardDismissSgmentedControl.selectedSegmentIndex {
        case 0:
            configuration.keyboardDismissMode = .none
        case 1:
            configuration.keyboardDismissMode = .onDrag
        case 2:
            configuration.keyboardDismissMode = .interactive
        default:
            configuration.keyboardDismissMode = .none
        }
        
        configuration.viewHighlightBorder = self.scrollViewHighlightBorderSwitch.isOn
        
        switch self.keyboardManagementAlignmentSegmentedControl.selectedSegmentIndex {
        case 0:
            configuration.attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointTopAlongCenter, .visibleRect))
        case 1:
            configuration.attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenter, .visibleRect))
        case 2:
            configuration.attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenterAlongBottom, .visibleRect))
        case 3:
            configuration.attributedAlignment.path = .moveTo(.toRect(.visibleRect))
        default:
            configuration.attributedAlignment.path = .automatic
        }
        
        configuration.attributedAlignment.calmArea = self.quiescentAreaSwitch.isOn
        
        if self.quiescentAreaSwitch.isOn, let _text = self.quiescentAreaHeightTextField.text, _text.count > 0
        {
            let numberFormatter = NumberFormatter()
            
            if let _number = numberFormatter.number(from: _text)
            {
                configuration.attributedAlignment.calmAreaSize = CGSize(width: CGFloat.infinity, height: CGFloat(_number.floatValue))
            }
        }
        
        return configuration
    }
}
