//
//  SettingsViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 28.03.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


protocol SettingsActiveConfigurationClientProtocol
{
    var activeConfiguration: UIConfiguration? { get set }
}


class SettingsViewController: ViewController, PresentableViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.keyboardManagement = KeyboardManagement()
        
        if let _keyboardManagement = self.keyboardManagement
        {
            if #available(iOS 11.0, *)
            {
                _keyboardManagement.scrollView?.contentInsetAdjustmentBehavior = .always
            }
            
            _keyboardManagement.scrollView = self.scrollView
            _keyboardManagement.isEnabled = true
        }
        
        (self.view as? SettingsView)?.onHideClosure = { [weak self] in
            if let onHideClosure = self?.onHideClosure
            {
                onHideClosure()
            }
        }
        (self.view as? SettingsView)?.onShowClosure = { [weak self] (_ configuration: UIConfiguration) in
            if let onShowClosure = self?.onShowClosure
            {
                onShowClosure(configuration)
            }
        }
    }
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var onHideClosure: (() -> Void)? = nil
    var onShowClosure: ((_ configuration: UIConfiguration) -> Void)? = nil
}
