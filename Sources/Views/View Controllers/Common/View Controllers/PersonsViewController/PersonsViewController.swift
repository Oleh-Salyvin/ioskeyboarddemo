//
//  PersonsViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 16.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class PersonsViewController: ViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        let configuration = self.settings()
        
        if self.keyboardManagement != nil
        {
            self.scrollView.bounces = configuration.bounceOnScroll
            self.scrollView.bouncesZoom = configuration.bounceOnZoom
            self.scrollView.alwaysBounceVertical = configuration.bounceVertically
            self.scrollView.alwaysBounceHorizontal = configuration.bounceHorizontally
            self.scrollView.keyboardDismissMode = configuration.keyboardDismissMode
        }
        
        self.buildUI()
    }
    
    override func settings() -> UIConfiguration
    {
        let configuration = UIConfiguration()
        
        configuration.viewHighlightBorder = true
        
        configuration.attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointLeftAlongCenter, .visibleRect))
        
        return configuration
    }
    
    
    //MARK: - Private
    
    private func buildUI()
    {
        if let _persons = self.personGroupContext?.person(number: 10, offset: 40)
        {
            for personElement in _persons
            {
                let builder = PersonViewControllerBuilder(personEntity: personElement)
                builder.construct(index: 0)
                
                let vc = builder.result()
                
                vc.keyboardManagement = self.keyboardManagement
                
                self.addChild(vc)
                
                self.show(view: vc.view, in: self.scrollView.subviews.first!)
            }
        }
    }
    
    private func show(view: UIView, in contentView: UIView)
    {
        if (contentView.subviews.isEmpty == true)
        {
            contentView.addSubview(view)
            
            self.contentViewTrailingConstraint = NSLayoutConstraint(item: contentView, attribute: .trailingMargin, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
            
            contentView.addConstraints([
                NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 0),
                
                NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leadingMargin, multiplier: 1, constant: 0),
                self.contentViewTrailingConstraint!
                ])
        }
        else
        {
            self.contentViewTrailingConstraint?.isActive = false
            
            let attachToView = contentView.subviews.last!
            contentView.insertSubview(view, aboveSubview: attachToView)
            
            self.contentViewTrailingConstraint = NSLayoutConstraint(item: contentView, attribute: .trailingMargin, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
            
            contentView.addConstraints([
                NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 0),
                
                NSLayoutConstraint(item: view, attribute: .leadingMargin, relatedBy: .equal, toItem: attachToView, attribute: .trailing, multiplier: 1, constant: space),
                self.contentViewTrailingConstraint!
                ])
        }
    }
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    private var contentViewTrailingConstraint: NSLayoutConstraint?
    
    var space: CGFloat = 5
    
    var personGroupContext: PersonGroupContext? = nil
}
