//
//  PersonViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 09.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement

class PersonViewController: ViewController, RedirectDelegate
{
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var ageTextField: UITextField!
    
    var rootView: PersonView {
        return self.view as! PersonView
    }
    
    var personEntity: PersonEntity? = nil {
        didSet {
            self.buildUI()
        }
    }
    
    func buildUI()
    {
        self.rootView.redirectDelegate = self
        
        self.loadViewIfNeeded()
        
        self.firstNameTextField.text = self.personEntity?.firstName
        
        self.lastNameTextField.text = self.personEntity?.lastName
        
        self.ageTextField.text = self.personEntity?.age
    }
    
    //MARK: - RedirectDelegate
    
    func accessRedirected(redirectView: UIView, visitor: inout KMViewVisitorProtocol)
    {
        visitor.visit(view: redirectView, firstResponders: self.availableResponders())
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.rootView.redirectDelegate = self
        
        self.buildUI()
    }
    
    @IBAction func onFirstNameButton(_ sender: UIButton)
    {
        self.firstNameTextField.isEnabled = !self.firstNameTextField.isEnabled
    }
    
    @IBAction func onLastNameButton(_ sender: Any)
    {
        self.lastNameTextField.isEnabled = !self.lastNameTextField.isEnabled
    }
    
    @IBAction func onAgeButton(_ sender: UIButton)
    {
        self.ageTextField.isEnabled = !self.ageTextField.isEnabled
    }
    
    func availableResponders() -> Array<UIView>
    {
        var views: Array<UIView> = []
        
        if self.firstNameTextField.isEnabled
        {
            views.append(self.firstNameTextField)
        }
        
        if self.lastNameTextField.isEnabled
        {
            views.append(self.lastNameTextField)
        }
        
        if self.ageTextField.isEnabled
        {
            views.append(self.ageTextField)
        }
        
        return views
    }
}
