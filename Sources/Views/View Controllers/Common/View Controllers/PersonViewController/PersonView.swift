//
//  PersonView.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 08.06.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class PersonView: UIView, KMVisitorComponentViewProtocol
{
    weak var redirectDelegate: RedirectDelegate?
    
    
    //MARK: - KMVisitorComponentViewProtocol
    
    func accept(visitor: inout KMViewVisitorProtocol)
    {
        self.redirectDelegate?.accessRedirected(redirectView: self, visitor: &visitor)
    }
}
