//
//  TextViewCell.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 1/9/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class TextViewCell: UITableViewCell, UITextViewDelegate, KMVisitorComponentViewProtocol
{
    @IBOutlet weak var textView: UITextView!
    
    //MARK: - KMVisitorComponentViewProtocol
    
    func accept(visitor: inout KMViewVisitorProtocol)
    {
        visitor.visit(view: self, firstResponders: [self.textView])
    }
    
    //MARK: - UITextViewDelegate
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        return true
    }
}
