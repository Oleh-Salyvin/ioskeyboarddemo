//
//  TextFieldCell.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 12/12/16.
//  Copyright © 2016 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class TextFieldCell: UITableViewCell, UITextFieldDelegate, KMVisitorComponentViewProtocol
{
    @IBOutlet weak var textField: UITextField!
    
    weak var delegate: DemoNextResponderDelegate? = nil
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
        
        self.textField.text = nil
    }
    
    //MARK: - KMVisitorComponentViewProtocol
    
    func accept(visitor: inout KMViewVisitorProtocol)
    {
        visitor.visit(view: self, firstResponders: [self.textField])
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return self.delegate!.nextFirstResponder(currentCell: self)
    }
}
