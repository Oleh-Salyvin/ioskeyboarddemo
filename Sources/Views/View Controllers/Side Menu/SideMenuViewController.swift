//
//  SideMenuViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 2/5/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


class SideMenuViewController: UIViewController
{
    @IBAction func onTransitionOnSwiftArticle(_ sender: Any)
    {
        UIApplication.shared.openURL(URL(string: "http://mathewsanders.com/custom-menu-transitions-in-swift/")!)
    }
    
    @IBAction func onOpenTableViewCellAnimationArticle(_ sender: Any)
    {
        UIApplication.shared.openURL(URL(string: "http://www.appcoda.com/view-animation-in-swift/")!)
    }
    
    @IBAction func SettingDeviceOrientatinInSwiftiOS(_ sender: Any)
    {
        UIApplication.shared.openURL(URL(string: "http://stackoverflow.com/questions/25651969/setting-device-orientation-in-swift-ios")!)
    }
    
    @IBAction func onDismiss(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
