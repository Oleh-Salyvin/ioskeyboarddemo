//
//  SideMenuTransitionManager.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 2/5/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


class SideMenuTransitionManager: UIPercentDrivenInteractiveTransition,
UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate
{
    private var interactive = false
    
    private var enterPanGesture: UIScreenEdgePanGestureRecognizer?
    
    var sourceViewController: UIViewController? {
        didSet {
            self.enterPanGesture = UIScreenEdgePanGestureRecognizer()
            self.enterPanGesture?.addTarget(self, action: #selector(handleOnstagePan))
            self.enterPanGesture?.edges = UIRectEdge.left
            self.sourceViewController?.view.addGestureRecognizer(self.enterPanGesture!)
        }
    }
    
    //MARK: - UIViewControllerAnimatedTransitioning
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        let container = transitionContext.containerView
        
        let screens: (from:UIViewController, to:UIViewController) = (
            transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!,
            transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        )
        
        screens.to.view.transform = CGAffineTransform(translationX: -container.frame.width, y: 0)
        
        container.addSubview(screens.from.view)
        container.addSubview(screens.to.view)
        
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration,
                       delay: 0.0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.8,
                       options: [],
                       animations:
            {
                screens.to.view.transform = CGAffineTransform.identity
        }, completion:
            { finished in
                if transitionContext.transitionWasCancelled
                {
                    transitionContext.completeTransition(false)
                    
                    UIApplication.shared.keyWindow?.addSubview(screens.from.view)
                }
                else
                {
                    transitionContext.completeTransition(true)
                    
                    UIApplication.shared.keyWindow?.addSubview(screens.to.view)
                }
        })
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval
    {
        return 0.25
    }
    
    func animationEnded(_ transitionCompleted: Bool)
    {
        
    }
    
    //MARK: - UIViewControllerTransitioningDelegate
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning?
    {
        return self.interactive ? self : nil
    }
    
    @objc private func handleOnstagePan(pan: UIScreenEdgePanGestureRecognizer)
    {
        let translation = pan.translation(in: pan.view!)
        let percentageOfTransition = translation.x / pan.view!.bounds.width * 0.5
        
        switch (pan.state)
        {
        case UIGestureRecognizer.State.began:
            self.interactive = true
            
            self.sourceViewController?.performSegue(withIdentifier: "showBookmarksSegueIdentifier", sender: self)
            break
            
        case UIGestureRecognizer.State.changed:
            update(percentageOfTransition)
            break
            
        default:// .end .cancelled .failed
            self.interactive = false
            
            if (percentageOfTransition > 0.2)
            {
                finish()
            }
            else
            {
                cancel()
            }
        }
    }
}
