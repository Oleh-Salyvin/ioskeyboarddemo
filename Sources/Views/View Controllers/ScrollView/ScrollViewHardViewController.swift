//
//  ScrollViewHardViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 08.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class ScrollViewHardViewController: ViewController
{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    var viewContext: ScrollViewLightMixViewContext = ViewModel.sharedInstance.context(name: .ScrollViewLightMix) as! ScrollViewLightMixViewContext
    
    private var keyboardManagementContext = KeyboardManagementContext()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let configuration = self.configuration()
        
        self.keyboardManagement = KeyboardManagement()
        
        if self.keyboardManagement != nil
        {
            let attributedAlignment = configuration.attributedAlignment
            attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenter, .visibleRect))
            self.keyboardManagementContext.attributedAlignment = attributedAlignment
            
            self.keyboardManagement!.scrollView = self.scrollView
            self.keyboardManagement!.notificationDelegate = self.keyboardManagementContext
            self.keyboardManagement!.isEnabled = true
            self.keyboardManagement!.isPipelineEnabled = true
        }
        
        super.configure(scrollView: self.scrollView, configuration: configuration)
        
        if #available(iOS 11.0, *)
        {
            self.scrollView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior(rawValue: configuration.contentInsetAdjustmentBehavior.rawValue)!
        }
        
        let builder = ScrollViewHardViewControllerBuilder(viewController: self, scrollView: self.scrollView, contentView: self.contentView, viewModel: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewLightMixViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.configuration()
        
        let builder = ScrollViewHardViewControllerBuilder(viewController: self, scrollView: self.scrollView, contentView: self.contentView, viewModel: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewLightMixViewController(builder, workType: .configure)
    }
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        configuration.contentInsetAdjustmentBehavior = .always
        
        return configuration
    }
}
