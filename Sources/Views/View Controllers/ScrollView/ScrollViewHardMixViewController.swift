//
//  ScrollViewHardMixViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 14.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class ScrollViewHardMixViewController: ViewController
{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    var viewContext: ScrollViewHardMixViewContext = ViewModel.sharedInstance.context(name: .ScrollViewHardMix) as! ScrollViewHardMixViewContext
    
    private var keyboardManagementContext = KeyboardManagementContext()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let configuration = self.configuration()
        
        self.keyboardManagement = KeyboardManagement()
        
        if self.keyboardManagement != nil
        {
            let attributedAlignment = configuration.attributedAlignment
            attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenter, .visibleRect))
            self.keyboardManagementContext.attributedAlignment = attributedAlignment
            
            self.keyboardManagement!.scrollView = self.scrollView
            self.keyboardManagement!.notificationDelegate = self.keyboardManagementContext
            self.keyboardManagement!.isEnabled = true
            self.keyboardManagement!.isPipelineEnabled = true
        }
        
        super.configure(scrollView: self.scrollView, configuration: configuration)
        
        self.scrollView.delegate = self
        
        if #available(iOS 11.0, *)
        {
            self.scrollView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior(rawValue: configuration.contentInsetAdjustmentBehavior.rawValue)!
        }
        
        let builder = ScrollViewHardMixViewControllerBuilder(viewController: self, scrollView: self.scrollView, contentView: self.contentView, viewContext: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewHardMixViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.configuration()
        
        let builder = ScrollViewHardMixViewControllerBuilder(viewController: self, scrollView: self.scrollView, contentView: self.contentView, viewContext: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewHardMixViewController(builder, workType: .configure)
    }
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        configuration.contentInsetAdjustmentBehavior = .scrollableAxes
        configuration.hidesBarsOnTap = false
        
        return configuration
    }
}
