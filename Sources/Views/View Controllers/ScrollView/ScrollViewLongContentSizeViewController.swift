//
//  ScrollViewLongContentSizeViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 21.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class ScrollViewLongContentSizeViewController: ViewController, ScrollViewAlignmentContextDelegate
{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var subTextField1: UITextField!
    @IBOutlet weak var subTextField2: UITextField!
    @IBOutlet weak var subTextField3: UITextField!
    
    private var viewContext: ScrollViewLongContentSizeViewContext = ViewModel.sharedInstance.context(name: .ScrollViewLongContentSize) as! ScrollViewLongContentSizeViewContext
    
    private var keyboardManagementContext = KeyboardManagementContext()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let configuration = self.configuration()
        
        self.keyboardManagement = KeyboardManagement()
        
        if self.keyboardManagement != nil
        {
            let attributedAlignment = configuration.attributedAlignment
            attributedAlignment.path = .automatic
            self.keyboardManagementContext.attributedAlignment = attributedAlignment
            self.keyboardManagementContext.delegate = self
            
            self.keyboardManagement!.scrollView = self.scrollView
            self.keyboardManagement!.delegate = self.keyboardManagementContext
            self.keyboardManagement!.notificationDelegate = self.keyboardManagementContext
            self.keyboardManagement!.isEnabled = true
            self.keyboardManagement!.isPipelineEnabled = true
        }
        
        self.scrollView.delegate = self
        
        super.configure(scrollView: self.scrollView, configuration: configuration)
        
        if #available(iOS 11.0, *)
        {
            self.scrollView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior(rawValue: configuration.contentInsetAdjustmentBehavior.rawValue)!
        }
        
        for (index, view) in self.firstResponders().enumerated()
        {
            if view is UITextInput
            {
                (view as! UITextField).delegate = self
                (view as! UITextField).placeholder = String(index)
            }
        }
        
        let builder = ScrollViewLongContentSizeViewControllerBuilder(self, scrollView: self.scrollView, viewModel: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewLongContentSizeViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.configuration()
        
        let builder = ScrollViewLongContentSizeViewControllerBuilder(self, scrollView: self.scrollView, viewModel: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewLongContentSizeViewController(builder, workType: .configure)
    }
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        configuration.contentInsetAdjustmentBehavior = .scrollableAxes
        configuration.attributedAlignment.path = .automatic
        
        return configuration
    }
    
    func firstResponders() -> Array<UIView>
    {
        return self.scrollView.subviews[0].subviews.filter { (view) -> Bool in
            return view is UITextInput
        } + [self.subTextField1, self.subTextField2, self.subTextField3]
    }
}
