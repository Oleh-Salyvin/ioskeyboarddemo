//
//  ScrollViewMenuViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 2/5/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


class ScrollViewMenuViewController: PresenterViewController
{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewLightButton: UIButton!
    @IBOutlet weak var scrollViewLightFullScreenButton: UIButton!
    @IBOutlet weak var scrollViewLightMixButton: UIButton!
    @IBOutlet weak var scrollViewHardMixButton: UIButton!
    
    var transitionManager = SideMenuTransitionManager()
    
    var destinationConfiguration: UIConfiguration? = nil
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.keyboardManagement = nil
        
        let configuration = self.activeConfiguration != nil ? self.activeConfiguration! : self.settings()
        
        let builder = ScrollViewMenuViewControllerBuilder(viewController: self, configuration: configuration)
        self.uiDirector.constructScrollViewMenuViewController(builder, workType: .construct)
        
        self.transitionManager.sourceViewController = self
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.activeConfiguration != nil ? self.activeConfiguration! : self.settings()
        
        let builder = ScrollViewMenuViewControllerBuilder(viewController: self, configuration: configuration)
        self.uiDirector.constructScrollViewMenuViewController(builder, workType: .configure)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (segue.identifier == "showBookmarksSegueIdentifier")
        {
            segue.destination.transitioningDelegate = self.transitionManager
        }
        else if var _protocol = segue.destination as? SettingsActiveConfigurationClientProtocol
        {
            _protocol.activeConfiguration = self.destinationConfiguration
            self.destinationConfiguration = nil
        }
    }
    
    @IBAction func onShowBookmarks(_ sender: Any)
    {
        performSegue(withIdentifier: "showBookmarksSegueIdentifier", sender: sender)
    }
    
    @IBAction func unwindToViewController(sender: UIStoryboardSegue)
    {
        // Need for dismiss SideMenu
    }
    
    @IBAction func onLongPressShowConfiguration(_ sender: UILongPressGestureRecognizer)
    {
        var segueIdentifier: String = String()
        
        if (sender.view == self.scrollViewLightButton)
        {
            segueIdentifier = "showUIScrollView"
        }
        else if (sender.view == self.scrollViewLightFullScreenButton)
        {
            segueIdentifier = "showUIScrollViewInsets"
        }
        else if (sender.view == self.scrollViewLightMixButton)
        {
            segueIdentifier = "showScrollViewHorizontal"
        }
        else if (sender.view == self.scrollViewHardMixButton)
        {
            segueIdentifier = "showScrollViewComplex"
        }
        
        if (sender.state == .began)
        {
            let settingsStoryboard = UIStoryboard(name: "Settings", bundle: nil)
            
            let vc = settingsStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            
            vc.onHideClosure = { [weak self] in
                self?.dismiss(animated: true, completion: {})
            }
            vc.onShowClosure = { [weak self] (_ configuration: UIConfiguration) in
                self?.destinationConfiguration = configuration
                
                self?.dismiss(animated: true, completion: { [weak self] in
                    self?.performSegue(withIdentifier: segueIdentifier, sender: vc)
                })
            }
            
            self.present(vc, animated: true, completion: {})
        }
    }
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        configuration.automaticallyAdjustsScrollViewInsets = true
        configuration.hidesBarsOnTap = false
        
        return configuration
    }
}

