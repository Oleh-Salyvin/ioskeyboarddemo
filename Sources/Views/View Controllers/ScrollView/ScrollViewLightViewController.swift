//
//  ScrollViewLightViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 12/5/16.
//  Copyright © 2016 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class ScrollViewLightViewController: ViewController, ScrollViewAlignmentContextDelegate
{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var control1: UITextField!
    @IBOutlet weak var control2: UITextField!
    @IBOutlet weak var control3: UITextField!
    @IBOutlet weak var control4: UITextField!
    @IBOutlet weak var control5: UITextField!
    @IBOutlet weak var control6: UITextField!
    @IBOutlet weak var control7: UITextField!
    @IBOutlet weak var control8: UITextField!
    @IBOutlet weak var control9: UITextField!
    @IBOutlet weak var control10: UITextField!
    @IBOutlet weak var control11: UITextField!
    @IBOutlet weak var control12: UITextField!
    @IBOutlet weak var control13: UITextField!
    @IBOutlet weak var control14: UITextField!
    @IBOutlet weak var control15: UITextField!
    @IBOutlet weak var control16: UITextField!
    
    private var keyboardManagementContext = KeyboardManagementContext()
    
    private var viewContext: ScrollViewLightViewContext = ViewModel.sharedInstance.context(name: .ScrollViewLight) as! ScrollViewLightViewContext
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let configuration = self.configuration()
        
        self.keyboardManagement = KeyboardManagement()
        
        if self.keyboardManagement != nil
        {
            let attributedAlignment = configuration.attributedAlignment
            attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenter, .visibleRect))
            self.keyboardManagementContext.attributedAlignment = attributedAlignment
            self.keyboardManagementContext.delegate = self
            
            self.keyboardManagement!.scrollView = self.scrollView
            self.keyboardManagement!.delegate = self.keyboardManagementContext
            self.keyboardManagement!.notificationDelegate = self.keyboardManagementContext
            self.keyboardManagement!.isEnabled = true
            self.keyboardManagement!.isPipelineEnabled = true
        }
        
        self.scrollView.delegate = self
        
        super.configure(scrollView: self.scrollView, configuration: configuration)
        
        if #available(iOS 11.0, *)
        {
            self.scrollView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior(rawValue: configuration.contentInsetAdjustmentBehavior.rawValue)!
        }
        
        let builder = ScrollViewLightViewControllerBuilder(self, scrollView: self.scrollView, viewContext: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewLightViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.configuration()
        
        let builder = ScrollViewLightViewControllerBuilder(self, scrollView: self.scrollView, viewContext: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewLightViewController(builder, workType: .configure)
    }
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        configuration.contentInsetAdjustmentBehavior = .always
        
        configuration.hidesBarsOnTap = true
        
        return configuration
    }
    
    //MARK: - ScrollViewAlignmentContextDelegate
    
    func firstResponders() -> Array<UIView>
    {
        if self.isMember(of: ScrollViewLightViewController.self)
        {
            return [self.control1, self.control2, self.control3, self.control4,self.control5, self.control6, self.control7, self.control8, self.control9, self.control10, self.control11, self.control12, self.control13, self.control14, self.control15, self.control16]
        }
        
        return self.contentView.subviews
    }
}
