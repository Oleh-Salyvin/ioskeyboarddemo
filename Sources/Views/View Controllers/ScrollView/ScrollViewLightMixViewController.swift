//
//  ScrollViewLightMixViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 12/5/16.
//  Copyright © 2016 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class ScrollViewLightMixViewController: ScrollViewLightViewController
{
    var viewContext: ScrollViewLightMixViewContext  = ViewModel.sharedInstance.context(name: .ScrollViewLightMix) as! ScrollViewLightMixViewContext
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.keyboardManagement?.isEnabled = false
        
        let configuration = self.configuration()
        
        if #available(iOS 11.0, *)
        {
            self.scrollView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior(rawValue: configuration.contentInsetAdjustmentBehavior.rawValue)!
        }
        
        let builder = ScrollViewLightMixViewControllerBuilder(self, scrollView: self.scrollView, viewModel: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewLightFullScreenViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.configuration()
        
        let builder = ScrollViewLightMixViewControllerBuilder(self, scrollView: self.scrollView, viewModel: self.viewContext, configuration: configuration)
        self.uiDirector.constructScrollViewLightFullScreenViewController(builder, workType: .configure)
        
        self.keyboardManagement?.isEnabled = true
        
        self.firstResponders()[7].becomeFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.view.endEditing(true)
        
        super.viewDidAppear(animated)
        
        self.firstResponders()[7].becomeFirstResponder()
    }
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        return configuration
    }
}
