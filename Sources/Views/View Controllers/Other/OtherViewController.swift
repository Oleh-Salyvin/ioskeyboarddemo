//
//  OtherViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 05.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class OtherViewController: ViewController
{
    @IBOutlet weak var wifiAddrres: UILabel!
    @IBOutlet weak var shapeLabel: UILabel!
    
    private var colorView: ColorView = ColorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    
    private var layerPositionAnimation = LayerPositionAnimation()
    
    private var colorLayerDelegate = ColorLayerDelegate()
    
    private var otherViewContext: OtherViewContext = ViewModel.sharedInstance.context(name: .ScrollViewOther) as! OtherViewContext
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        
        let tlVC = LabelViewController(nibName: nil, bundle: nil)
        tlVC.loadViewIfNeeded()
        tlVC.timeLabel.text = self.otherViewContext.timeNow
        
        let tfVC = TextFieldViewController(nibName: nil, bundle: nil)
        tfVC.loadViewIfNeeded()
        tfVC.textField.text = self.otherViewContext.textFieldLabel
        
        self.buildLayoutConstraints([tlVC, tfVC])
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        self.view.addSubview(self.colorView)
        self.colorView.backgroundColor = UIColor.yellow
        self.colorView.layer.position = CGPoint(x: self.colorView.bounds.midX, y: 250)
        
        CATransaction.commit()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.colorView.layer.delegate = self.colorLayerDelegate
        
        self.wifiAddrres.text = self.otherViewContext.wifiAddressLabel
        self.keyboardManagement?.isEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        self.updateColorView()
        self.animateColorView()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.keyboardManagement?.isEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        self.colorView.layer.delegate = nil
    }
    
    private func updateColorView()
    {
        if let _fromColor = self.colorView.backgroundColor
        {
            self.colorLayerDelegate.fromColor = _fromColor
        }
        
        var toColor: UIColor?
        
        if self.colorView.backgroundColor != UIColor.red
        {
            toColor = UIColor.red
        }
        else
        {
            toColor = UIColor.green
        }
        
        self.colorLayerDelegate.toColor = toColor!
        
        self.colorView.backgroundColor = toColor
    }
    
    private func animateColorView()
    {
        let y: CGFloat = self.colorView.layer.position.y
        let fromPosition: CGPoint = CGPoint(x: self.colorView.layer.bounds.midX, y: y)
        
        // Implicit animation triggered on changing "position" key
        let toPosition = CGPoint(x: self.view.bounds.width - self.colorView.bounds.midX, y: y)
        
        self.colorLayerDelegate.duration = 5
        self.colorLayerDelegate.fromPosition = fromPosition
        self.colorLayerDelegate.toPosition = toPosition
        
        self.colorView.layer.position = toPosition
    }
    
    private func buildLayoutConstraints(_ viewControllers: [UIViewController])
    {
        self.addChild(viewControllers[0])
        self.addChild(viewControllers[1])
        self.view.addSubview(viewControllers[0].view)
        self.view.addSubview(viewControllers[1].view)
        
        self.view.addConstraints([
            NSLayoutConstraint(item: viewControllers[0].view, attribute: .width, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .width, multiplier: 1, constant: 100),
            NSLayoutConstraint(item: viewControllers[0].view, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 200),
            NSLayoutConstraint(item: viewControllers[0].view, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 135)
            ])
        
        self.view.addConstraints([
            NSLayoutConstraint(item: viewControllers[1].view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 150),
            NSLayoutConstraint(item: viewControllers[1].view, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 165),
            NSLayoutConstraint(item: viewControllers[1].view, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 100)
            ])
    }
}


class ColorView: UIView
{
    override func action(for layer: CALayer, forKey event: String) -> CAAction?
    {
        return ColorLayerDelegate().action(for: self.layer, forKey: event)
    }
}
