//
//  StackViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 06.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


class StackViewController: UIViewController
{
    @IBOutlet weak var horizontalStackView: UIStackView!
    @IBOutlet weak var verticalStackView: UIStackView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.horizontalStackView.backgroundColor = #colorLiteral(red: 0.3301932216, green: 0.694529593, blue: 0.7950767875, alpha: 1)
        self.horizontalStackView.addArrangedSubview(loadViewControllerFromNib(text: "M"))
        self.horizontalStackView.addArrangedSubview(loadViewControllerFromNib(text: "V"))
        self.horizontalStackView.addArrangedSubview(loadViewControllerFromNib(text: "VM"))
        self.horizontalStackView.addArrangedSubview(loadViewControllerFromNib(text: "pattern"))
        
        self.verticalStackView.addArrangedSubview(loadViewControllerFromNib(text: "M"))
        self.verticalStackView.addArrangedSubview(loadViewControllerFromNib(text: "V"))
        self.verticalStackView.addArrangedSubview(loadViewControllerFromNib(text: "VM"))
        self.verticalStackView.addArrangedSubview(loadViewControllerFromNib(text: "pattern"))
    }
    
    private func loadViewControllerFromNib(text: String) -> UIView
    {
        let vc = LabelViewController(nibName: nil, bundle: nil)
        vc.loadViewIfNeeded()
        self.addChild(vc)
        
        vc.timeLabel.text = text
        
        return vc.view
    }
}
