//
//  TableViewLightTableViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 30.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import CoreData
import KeyboardManagement


class TableViewLightTableViewController: TableViewController, DemoNextResponderDelegate, UITableViewDataSourcePrefetching, NSFetchedResultsControllerDelegate, UITextFieldDelegate
{
    private var viewContext: PostViewContext = ViewModel.sharedInstance.context(name: .PostView) as! PostViewContext
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let configuration = self.configuration()
        
        self.keyboardManagement = KeyboardManagement()
        
        if self.keyboardManagement != nil
        {
            self.keyboardManagement!.scrollView = self.tableView
            self.keyboardManagement!.isEnabled = true
            
            if #available(iOS 10, *)
            {
                self.tableView.prefetchDataSource = self
            }
        }
        
        super.configure(scrollView: self.tableView, configuration: configuration)
        self.tableView.allowsSelection = false
        
        self.viewContext.fetchedResultsController?.delegate = self
        self.viewContext.performFetch()
        
        if (self.viewContext.fetchedObjects == nil)
        {
            print("The value of the property is nil if performFetch() hasn’t been called.")
        }
        else if let configuration = self.viewContext.fetchedObjects, configuration.count < 50
        {
            self.viewContext.insertPosts(number: 50 - configuration.count)
        }
        
        self.viewContext.save { (error) in
            print(error.debugDescription + ", Saved!")
        }
        
        let builder = TableViewLightTableViewControllerBuilder(viewController: self, tableView: self.tableView!, configuration: configuration)
        self.uiDirector.constructTableViewLightTableViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.activeConfiguration != nil ? self.activeConfiguration! : self.settings()
        
        let builder = TableViewLightTableViewControllerBuilder(viewController: self, tableView: self.tableView!, configuration: configuration)
        self.uiDirector.constructTableViewLightTableViewController(builder, workType: .configure)
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if (textField.keyboardType == .numberPad) || (textField.keyboardType == .phonePad)
        {
            let builder = KeyboardManagementToolBarBuilder.init(self.view, target: self)
            builder.construct()
            textField.inputAccessoryView = builder.result()
        }
        
        return self.keyboardManagement!.shouldBeginEditing(view: textField)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if let _textField = self.keyboardManagement?.next()
        {
            _textField.becomeFirstResponder()
            return false
        }
        
        return true
    }
    
    //MARK: - NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
    {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?)
    {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                let cell = self.tableView.cellForRow(at: indexPath) as! TextFieldCell
                
                self.configure(cell: cell, indexPath: indexPath)
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
    {
        self.tableView.endUpdates()
    }
    
    //MARK: - UITableViewDataSourcePrefetching
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath])
    {
        //LOG("\(indexPaths)")
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath])
    {
        //LOG("\(indexPaths)")
    }
    
    //MARK: - TextFieldCellDelegate
    
    func nextFirstResponder(currentCell: TextFieldCell) -> Bool
    {
        return self.keyboardManagement?.next() != nil ? true : false
    }
    
    //MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.viewContext.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.viewContext.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell") as! TextFieldCell
        
        self.configure(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
    func configure(cell: TextFieldCell, indexPath: IndexPath)
    {
        cell.textField.placeholder = self.viewContext.textPlaceholder(indexPath: indexPath)
        cell.textField.delegate = self
        cell.delegate = self
    }
    
    //MARK: - SettingsActiveConfigurationClientProtocol
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        if #available(iOS 11.0, *)
        {}
        else
        {
            configuration.automaticallyAdjustsScrollViewInsets = true
        }
        
        return configuration
    }
}
