//
//  TableViewMenuViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 11.05.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class TableViewMenuViewController: PresenterViewController
{
    var destinationConfiguration: UIConfiguration? = nil
    
    @IBOutlet weak var tableViewLightButton: UIButton!
    @IBOutlet weak var tableViewLightMixButton: UIButton!
    @IBOutlet weak var tableViewControllerLightButton: UIButton!
    
    override func viewDidLoad()
    {
        self.keyboardManagement = nil
        
        super.viewDidLoad()
        
        //let builder = CollectionViewMenuViewControllerBuilder(viewController: self, configuration: self.settings())
        //self.director.constructCollectionViewMenuViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.activeConfiguration != nil ? self.activeConfiguration! : self.settings()
        
        let builder = TableViewMenuViewControllerBuilder(viewController: self, configuration: configuration)
        self.uiDirector.constructTableViewMenuViewController(builder, workType: .configure)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if var _protocol = segue.destination as? SettingsActiveConfigurationClientProtocol
        {
            _protocol.activeConfiguration = self.destinationConfiguration
            self.destinationConfiguration = nil
        }
    }
    
    @IBAction func onLongPressShowConfiguration(_ sender: UILongPressGestureRecognizer)
    {
        var segueIdentifier: String = String()
        
        if (sender.view == self.tableViewLightButton)
        {
            segueIdentifier = "showTableViewLight"
        }
        else if (sender.view == self.tableViewLightMixButton)
        {
            segueIdentifier = "showTableViewLightMix"
        }
        else if (sender.view == self.tableViewControllerLightButton)
        {
            segueIdentifier = "showTableViewControllerLight"
        }
        
        if (sender.state == .began)
        {
            let settingsStoryboard = UIStoryboard(name: "Settings", bundle: nil)
            
            let vc = settingsStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            
            vc.onHideClosure = { [weak self] in
                self?.dismiss(animated: true, completion: {})
            }
            
            vc.onShowClosure = { [weak self] (_ configuration: UIConfiguration) in
                self?.destinationConfiguration = configuration
                
                self?.dismiss(animated: true, completion: { [weak self] in
                    self?.performSegue(withIdentifier: segueIdentifier, sender: vc)
                })
            }
            
            self.present(vc, animated: true, completion: {})
        }
    }
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        configuration.automaticallyAdjustsScrollViewInsets = true
        configuration.hidesBarsOnTap = false
        
        return configuration
    }
}
