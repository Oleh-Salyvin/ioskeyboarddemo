//
//  TableViewLightViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 12/12/16.
//  Copyright © 2016 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class TableViewLightViewController: WarmTableViewController, DemoNextResponderDelegate
{
    var viewContext: TableViewLightViewContext = ViewModel.sharedInstance.context(name: .TableViewLight) as! TableViewLightViewContext
    
    private var keyboardManagementContext = KeyboardManagementContext()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let configuration = self.configuration()
        
        self.keyboardManagement = KeyboardManagement()
        
        if self.keyboardManagement != nil
        {
            let attributedAlignment = configuration.attributedAlignment
            attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenter, .visibleRect))
            self.keyboardManagementContext.attributedAlignment = attributedAlignment
            
            self.keyboardManagement!.scrollView = self.tableView
            self.keyboardManagement!.notificationDelegate = self.keyboardManagementContext
            self.keyboardManagement!.isEnabled = true
            self.keyboardManagement!.isPipelineEnabled = true
            
            self.tableView.bounces = configuration.bounceOnScroll
            self.tableView.bouncesZoom = configuration.bounceOnZoom
            self.tableView.alwaysBounceVertical = configuration.bounceVertically
            self.tableView.alwaysBounceHorizontal = configuration.bounceHorizontally
            self.tableView.keyboardDismissMode = configuration.keyboardDismissMode
            self.tableView.allowsSelection = false
        }
        
        super.configure(scrollView: self.tableView, configuration: configuration)
        
        let builder = TableViewLightViewControllerBuilder(viewController: self, tableView: self.tableView!, viewContext: self.viewContext, configuration: configuration)
        self.uiDirector.constructTableViewLightViewController(builder, workType: .construct)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let configuration = self.configuration()
        
        let builder = TableViewLightViewControllerBuilder(viewController: self, tableView: self.tableView!, viewContext: self.viewContext, configuration: configuration)
        self.uiDirector.constructTableViewLightViewController(builder, workType: .configure)
        
//        AnimatorTableViewLaunch.animate(in: self.tableView) {
//            UIView.animate(withDuration: 0.65, animations: {
//                AnimatorTableViewVerticalList.animate(in: self.tableView)
//            })
//        }
    }
    
    //MARK: - TextFieldCellDelegate
    
    func nextFirstResponder(currentCell: TextFieldCell) -> Bool
    {
        return self.keyboardManagement?.next() != nil ? true : false
    }
    
    //MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 40
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let configuration = self.configuration()
        
        if (indexPath.row < 20)
        {
            let builder = TextFieldCellBuilder(tableView: tableView, viewModel: self.viewContext, for: indexPath, configuration: configuration)
            builder.construct()
            let cell = builder.result()
            cell.textField.delegate = self
            return cell
        }
        else
        {
            let builder = TextViewCellBuilder(tableView: tableView, viewModel: self.viewContext, for: indexPath, configuration: configuration)
            builder.construct()
            let cell = builder.result()
            cell.textView.delegate = self
            return cell
        }
    }
    
    override func settings() -> UIConfiguration
    {
        let configuration = super.settings()
        
        configuration.hidesBarsOnTap = true
        
        return configuration
    }
}
