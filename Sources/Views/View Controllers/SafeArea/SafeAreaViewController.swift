//
//  SafeAreaViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 08.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement


class SafeAreaViewController: ViewController, KeyboardManagementDelegate
{
    @IBOutlet weak var scrollView: UIScrollView!
    
    private var controls: Array<UIView> = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if self.keyboardManagement != nil
        {
            self.keyboardManagement!.scrollView = self.scrollView
            self.keyboardManagement!.delegate = self
        }
        
        let contentView = self.scrollView.subviews.first!
        
        self.controls += contentView.subviews
    }
    
    //MARK: - KeyboardManagementDelegate
    
    func previousFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
    {
        let index = self.controls.index(of: keyboardManagement.firstResponder as! UIControl)
        
        guard let _index = index, index != nil else {
            return keyboardManagement.firstResponder
        }
        
        if (_index == 0)
        {
            return self.controls.last
        }
        
        return self.controls[_index - 1]
    }
    
    func nextFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
    {
        let index = self.controls.index(of: keyboardManagement.firstResponder as! UIControl)
        
        guard index != nil else {
            return keyboardManagement.firstResponder
        }
        
        if (index! == self.controls.count - 1)
        {
            return self.controls.first
        }
        return self.controls[index! + 1]
    }
}
