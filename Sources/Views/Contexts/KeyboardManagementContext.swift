//
//  KeyboardManagementContext.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 05.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment

protocol ScrollViewAlignmentContextDelegate: class
{
    func firstResponders() -> Array<UIView>
}

final class KeyboardManagementContext: KeyboardManagementNotificationDelegate, KeyboardManagementDelegate
{
    required init() {}

    weak var delegate: ScrollViewAlignmentContextDelegate?

    var attributedAlignment: SVAttributedAlignment = SVAttributedAlignment()

    private let scrollViewAlignment = ScrollViewAlignment()

    //MARK: - KeyboardManagementNotificationDelegate

    func notification(notification: KMNotification, keyboardManagement: KeyboardManagement)
    {
        debugPrint("\(self) \(#function), \(notification.name)")

        if notification.name == UIResponder.keyboardWillShowNotification
        {
            self.attributedAlignment.contentInset.frameEnd = notification.frameEnd!

            self.attributedAlignment.animation = SVAnimation(duration: notification.duration!, options: notification.options!)

            let command = SVCommand(name: .willShow, attributedAlignment: self.attributedAlignment, scrollView: keyboardManagement.scrollView!, view: keyboardManagement.firstResponder!)

            self.scrollViewAlignment.handle(command: command)
        }
        else if notification.name == UIResponder.keyboardWillHideNotification
        {
            self.attributedAlignment.animation = SVAnimation(duration: notification.duration!, options: notification.options!)

            let command = SVCommand(name: .willHide, attributedAlignment: self.attributedAlignment, scrollView: keyboardManagement.scrollView!, view: keyboardManagement.firstResponder!)

            self.scrollViewAlignment.handle(command: command)
        }
    }

    //MARK: - KeyboardManagementDelegate

    func previousFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
    {
        guard keyboardManagement.firstResponder != nil else { return nil }
        guard let _firstResponders = self.delegate?.firstResponders() else { return nil }
        guard let _index = _firstResponders.index(of: keyboardManagement.firstResponder!) else { return nil }

        if _index == 0
        {
            return _firstResponders.last
        }

        return _firstResponders[_index - 1]
    }

    func nextFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
    {
        guard keyboardManagement.firstResponder != nil else { return nil }
        guard let _firstResponders = self.delegate?.firstResponders() else { return nil }
        guard let _index = _firstResponders.index(of: keyboardManagement.firstResponder!) else { return nil }

        if _index == _firstResponders.count - 1
        {
            return _firstResponders.first
        }

        return _firstResponders[_index + 1]
    }
}
