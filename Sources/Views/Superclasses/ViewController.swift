//
//  ViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 1/13/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class ViewController: UIViewController, SettingsActiveConfigurationClientProtocol, UITextFieldDelegate, UITextViewDelegate
{
    var keyboardManagement: KeyboardManagement?

    var uiDirector: UIDirector = UIDirector()

    //MARK: - UIScrollViewDelegate

    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        print("\(scrollView.contentOffset)")
    }

    //MARK: - UITextFieldDelegate

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        guard self.keyboardManagement != nil else { return false }

        if (textField.keyboardType == .numberPad || textField.keyboardType == .phonePad) && (UIDevice.current.model == "iPhone" || UIDevice.current.model == "iPod touch")
        {
            let builder = KeyboardManagementToolBarBuilder.init(self.view, target: self)
            builder.construct()
            textField.inputAccessoryView = builder.result()
        }

        return self.keyboardManagement!.shouldBeginEditing(view: textField)
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if let _textField = self.keyboardManagement?.next()
        {
            _textField.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }

        return false
    }

    //MARK: - UITextViewDelegate

    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        if (textView.keyboardType == .numberPad) || (textView.keyboardType == .phonePad)
        {
            let builder = KeyboardManagementToolBarBuilder.init(self.view, target: self)
            builder.construct()
            textView.inputAccessoryView = builder.result()
        }

        return self.keyboardManagement!.shouldBeginEditing(view: textView)
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool
    {
        return true
    }

    //MARK: - SettingsActiveConfigurationClientProtocol

    var activeConfiguration: UIConfiguration? = nil

    func settings() -> UIConfiguration
    {
        let configuration = UIConfiguration()

        configuration.viewHighlightBorder = true

        configuration.attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenter, .visibleRect))
        configuration.attributedAlignment.calmArea = false

        if #available(iOS 11.0, *)
        {
            configuration.automaticallyAdjustsScrollViewInsets = false
            configuration.contentInsetAdjustmentBehavior = .always
        }
        else
        {
            configuration.automaticallyAdjustsScrollViewInsets = false
        }

        return configuration
    }

    func configuration() -> UIConfiguration
    {
        return self.activeConfiguration != nil ? self.activeConfiguration! : self.settings()
    }

    //MARK: - First Responder Navigation Items

    @objc func onPreviousFirstResponder()
    {
        if let _firstResponder = self.keyboardManagement?.previous()
        {
            _firstResponder.becomeFirstResponder()
        }
    }

    @objc func onNextFirstResponder()
    {
        if let _firstResponder = self.keyboardManagement?.next()
        {
            _firstResponder.becomeFirstResponder()
        }
    }

    @objc func onHideKeyboard()
    {
        self.view.endEditing(true)
    }

    func configure(scrollView: UIScrollView, configuration: UIConfiguration)
    {
        scrollView.bounces = configuration.bounceOnScroll
        scrollView.bouncesZoom = configuration.bounceOnZoom
        scrollView.alwaysBounceVertical = configuration.bounceVertically
        scrollView.alwaysBounceHorizontal = configuration.bounceHorizontally
        scrollView.keyboardDismissMode = configuration.keyboardDismissMode
    }
}
