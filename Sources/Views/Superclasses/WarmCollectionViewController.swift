//
//  WarmCollectionViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 30.01.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit


class WarmCollectionViewController: ViewController
{
    @IBOutlet weak var collectionView: UICollectionView!
}
