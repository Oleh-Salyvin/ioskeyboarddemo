//
//  TableViewController.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 30.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit
import KeyboardManagement
import ScrollViewAlignment


class TableViewController: UITableViewController, SettingsActiveConfigurationClientProtocol
{
    var keyboardManagement: KeyboardManagement?
    
    var uiDirector: UIDirector = UIDirector()
    
    override open func viewDidLoad()
    {
        super.viewDidLoad()
        
        let builder = KeyboardManagementNavigationItemBuilder(target: self)
        builder.construct()
        self.navigationItem.rightBarButtonItems = builder.result()
    }
    
    //MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    //MARK: - UITableViewDataSource
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        return UITableViewCell()
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 0
    }
    
    //MARK: - SettingsActiveConfigurationClientProtocol
    
    var activeConfiguration: UIConfiguration? = nil
    
    func settings() -> UIConfiguration
    {
        let configuration = UIConfiguration()
        
        configuration.viewHighlightBorder = true
        configuration.attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointLeftAlongCenter, .visibleRect))
        
        if #available(iOS 11.0, *)
        {
            configuration.automaticallyAdjustsScrollViewInsets = true
            configuration.contentInsetAdjustmentBehavior = .always
        }
        else
        {
            configuration.automaticallyAdjustsScrollViewInsets = false
        }
        
        return configuration
    }
    
    func configuration() -> UIConfiguration
    {
        return self.activeConfiguration != nil ? self.activeConfiguration! : self.settings()
    }
    
    //MARK: - First Responder Navigation Items
    
    @objc func onPreviousFirstResponder()
    {
        if let _firstResponder = self.keyboardManagement?.previous()
        {
            _firstResponder.becomeFirstResponder()
        }
    }
    
    @objc func onNextFirstResponder()
    {
        if let _firstResponder = self.keyboardManagement?.next()
        {
            _firstResponder.becomeFirstResponder()
        }
    }
    
    @objc func onHideKeyboard()
    {
        self.view.endEditing(true)
    }
    
    func configure(scrollView: UIScrollView, configuration: UIConfiguration)
    {
        scrollView.bounces = configuration.bounceOnScroll
        scrollView.bouncesZoom = configuration.bounceOnZoom
        scrollView.alwaysBounceVertical = configuration.bounceVertically
        scrollView.alwaysBounceHorizontal = configuration.bounceHorizontally
        scrollView.keyboardDismissMode = configuration.keyboardDismissMode
    }
}
