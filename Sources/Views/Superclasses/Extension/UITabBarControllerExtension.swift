//
//  UITabBarControllerExtension.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 3/9/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


protocol UITabBarControllerRotateProtocol: class
{
    var shouldAutorotate: Bool { get }
    
    var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation { get }
    
    var supportedInterfaceOrientations: UIInterfaceOrientationMask { get }
}


extension UITabBarController: UITabBarControllerRotateProtocol
{
    override open var shouldAutorotate: Bool {
        get {
            if let visibleVC = self.selectedViewController {
                return visibleVC.shouldAutorotate
            }
            return super.shouldAutorotate
        }
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        get {
            if let visibleVC = self.selectedViewController {
                return visibleVC.preferredInterfaceOrientationForPresentation
            }
            return super.preferredInterfaceOrientationForPresentation
        }
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            if let visibleVC = self.selectedViewController {
                return visibleVC.supportedInterfaceOrientations
            }
            return super.supportedInterfaceOrientations
        }
    }
}

