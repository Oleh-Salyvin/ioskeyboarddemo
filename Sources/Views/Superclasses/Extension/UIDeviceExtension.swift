//
//  UIDeviceExtension.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 3/9/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


protocol UIDeviseRotateProtocol: class
{
    func forceRotate(to orientation: UIDeviceOrientation)
}


extension UIDevice: UIDeviseRotateProtocol
{
    func forceRotate(to orientation: UIDeviceOrientation)
    {
        UIDevice.current.setValue(orientation.rawValue, forKey: "orientation")
    }
}

