//
//  UINavigationControllerExtension.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 3/9/17.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit


protocol UINavigationControllerRotateProtocol: class
{
    var shouldAutorotate: Bool { get }
    
    var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation { get }
    
    var supportedInterfaceOrientations: UIInterfaceOrientationMask { get }
}


extension UINavigationController: UINavigationControllerRotateProtocol
{
    override open var shouldAutorotate: Bool {
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.shouldAutorotate
            }
            return super.shouldAutorotate
        }
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.preferredInterfaceOrientationForPresentation
            }
            return super.preferredInterfaceOrientationForPresentation
        }
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.supportedInterfaceOrientations
            }
            return super.supportedInterfaceOrientations
        }
    }
}

