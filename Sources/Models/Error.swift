//
//  Error.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 21.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


typealias doneWithErrorClosure = ((_ error: Error?) ->Void)
typealias doneClosure = (() -> Void)


protocol DemoError: Error
{
    var localizedMessage: String { get }
}


struct CoreDataSQLiteError: DemoError
{
    let localizedMessage: String
    
    let nsError: NSError?
}
