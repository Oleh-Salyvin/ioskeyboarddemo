//
//  NetworkClient.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 21.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


class NetworkClient
{
    func loadPerson(number: Int, offset: Int) -> Data?
    {
        var persons: PersonRawArray = []
        
        for index in offset..<offset+number
        {
            persons.append(PersonRaw(firstName: "firstName", lastName: "lastName \(index)", age: "age 3"))
        }
        
        return CodableDataSource().encode(persons: PersonRaws(persons: persons).personsDict)
    }
}
