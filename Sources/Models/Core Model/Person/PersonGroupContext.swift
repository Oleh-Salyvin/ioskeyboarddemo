//
//  PersonGroupContext.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 20.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


typealias PersonGroup = Array<PersonEntity>


class PersonGroupContext
{
    func person(number: Int, offset: Int) -> PersonGroup?
    {
        guard let _raw = CoreModel.sharedInstance.person(number: number, offset: offset) else {
            return nil
        }
        
        return _raw.persons.map({ (raw) -> PersonEntity in
            return PersonEntity(with: raw)
        })
    }
}
