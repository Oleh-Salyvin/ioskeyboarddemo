//
//  CodableDataSource.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 21.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


protocol CodableDataSourceProtocol: DataSourceProtocol
{
    func stringJSON(from data: Data) -> String?
}


class CodableDataSource: CodableDataSourceProtocol
{
    func encode(persons dict: [String : PersonRawArray]) -> Data?
    {
        do {
            return try JSONEncoder().encode(dict)
        } catch let error {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    func decode(from data: Data) -> PersonRaws?
    {
        do {
            return try JSONDecoder().decode(PersonRaws.self, from: data)
        } catch let error {
            print(error.localizedDescription)
        }
        
        return nil
    }
}


extension CodableDataSourceProtocol
{
    func stringJSON(from data: Data) -> String?
    {
        return String(data: data, encoding: .utf8)
    }
}
