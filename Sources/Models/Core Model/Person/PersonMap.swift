//
//  PersonsMapper.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 20.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


typealias PersonRawArray = Array<PersonRaw>


struct PersonRaws: Codable
{
    var persons: PersonRawArray = []
    
    var personsDict: Dictionary<String, PersonRawArray> {
        return [CodingKeys.persons.rawValue: self.persons]
    }
    
    enum CodingKeys: String, CodingKey
    {
        case persons
    }
}


struct PersonRaw: Codable
{
    let firstName: String
    let lastName: String
    let age: String
}
