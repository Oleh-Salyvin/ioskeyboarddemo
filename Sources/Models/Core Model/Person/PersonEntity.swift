//
//  PersonEntity.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 15.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation


struct PersonEntity
{
    let firstName: String
    
    let lastName: String
    
    let age: String
}


extension PersonEntity
{
    init(with raw: PersonRaw)
    {
        self.firstName = raw.firstName
        
        self.lastName = raw.lastName
        
        self.age = raw.age
    }
}
