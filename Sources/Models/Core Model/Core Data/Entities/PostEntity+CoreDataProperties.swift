//
//  PostEntity+CoreDataProperties.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 26.12.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//
//

import Foundation
import CoreData


extension PostEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PostEntity> {
        return NSFetchRequest<PostEntity>(entityName: "PostEntity")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var image: NSData?
    @NSManaged public var text: String?
    @NSManaged public var title: String?

}
