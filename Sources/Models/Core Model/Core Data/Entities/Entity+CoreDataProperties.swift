//
//  Entity+CoreDataProperties.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 19.01.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//
//

import Foundation
import CoreData


extension Entity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Entity> {
        return NSFetchRequest<Entity>(entityName: "Entity")
    }

    @NSManaged public var title: String?

}
