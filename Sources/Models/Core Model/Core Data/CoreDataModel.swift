//
//  CoreDataSQLiteProvider.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 26.12.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import Foundation
import CoreData


class CoreDataModel
{
    required init() throws
    {
        let modelName = "CoreDataModel"
        
        guard let modelURL = Bundle(for: CoreDataModel.self).url(forResource: modelName, withExtension: "momd") else {
            throw CoreDataSQLiteError(localizedMessage: "The file could not be located.", nsError: nil)
        }
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            throw CoreDataSQLiteError(localizedMessage: "The file could not be located.", nsError: nil)
        }
        
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: mom)
        
        let parentContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        parentContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsURL = urls.last!
        
        let storeURL = documentsURL.appendingPathComponent("\(modelName).sqlite")
        
        do {
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        }
        catch let error {
            throw CoreDataSQLiteError(localizedMessage: "There was an error creating or loading the application's saved data.", nsError: error as NSError)
        }
        
        self.parentContext = parentContext
        
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.parent = self.parentContext
        self.mainContext = managedObjectContext
    }
    
    let mainContext: NSManagedObjectContext
    
    private let parentContext: NSManagedObjectContext
    
    func save(_ closure: @escaping doneWithErrorClosure)
    {
        self.saveMainContext { (error) in
            guard error == nil else { closure(error)
                return
            }
            
            self.saveParentContext { (error) in
                guard error == nil else { closure(error)
                    return
                }
            }
        }
    }
    
    func saveMainContext(_ closure: @escaping doneWithErrorClosure)
    {
        self.save(context: self.mainContext, closure)
    }
    
    func saveParentContext(_ closure: @escaping doneWithErrorClosure)
    {
        self.save(context: self.parentContext, closure)
    }
    
    func save(context: NSManagedObjectContext, _ closure: @escaping doneWithErrorClosure)
    {
        if (context.hasChanges == true)
        {
            context.perform {
                do {
                    try context.save()
                }
                catch let error {
                    closure(CoreDataSQLiteError(localizedMessage: (error as NSError).localizedDescription, nsError: error as NSError))
                }
            }
        }
    }
}
