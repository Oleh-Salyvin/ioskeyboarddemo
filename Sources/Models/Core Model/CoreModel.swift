//
//  CoreModel.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 25.12.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import Foundation
import CoreData


class CoreModel
{
    static let sharedInstance = CoreModel()
    
    var coreDataModel: CoreDataModel? = {
        do {
            return try CoreDataModel()
        }
        catch let error {
        }
        return nil
    }()
    
    func save(_ closure: @escaping doneWithErrorClosure)
    {
        self.coreDataModel?.save { (error) in
            closure(error)
        }
    }
}


//MARK: - Person
extension CoreModel
{
    func personFetchedResultsController() -> NSFetchedResultsController<NSFetchRequestResult>?
    {
        guard self.coreDataModel != nil else { return nil }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PostEntity")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.coreDataModel!.mainContext, sectionNameKeyPath: nil, cacheName: nil)
    }
    
    func person(number: Int, offset: Int) -> PersonRaws?
    {
        if let _data = NetworkClient().loadPerson(number: number, offset: offset)
        {
            return CodableDataSource().decode(from: _data)
        }
        
        return nil
    }
    
    func insertPost() -> PostEntity?
    {
        guard self.coreDataModel != nil else { return nil }
        guard let entity = NSEntityDescription.entity(forEntityName: "PostEntity", in: self.coreDataModel!.mainContext) else { return nil }
        
        return PostEntity(entity: entity, insertInto: self.coreDataModel!.mainContext)
    }
}
